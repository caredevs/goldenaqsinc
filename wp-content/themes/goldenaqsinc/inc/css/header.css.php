<?php
	$tabbarVisibleWidth = carbon_get_theme_option('tabby_hide_windows_larger_than');
	$mobileMenuLayout = carbon_get_theme_option('tabby_menu_layout');
	$breakpoint = empty($tabbarVisibleWidth) ? '991' : $tabbarVisibleWidth;
	$isSticky = carbon_get_theme_option('tabby_sticky_header');
?>

<?php if (!empty($isSticky) == 'yes') : ?>
	@media(min-width: <?php echo $breakpoint+1; ?>px){
	body.tabby-sticky-header #page {
	<?php
		$topHeaderHeight = (int)carbon_get_theme_option('tabby_header_height_top') ? (int)carbon_get_theme_option('tabby_header_height_top') : '52';
	?>
		padding-top: <?php echo $topHeaderHeight; ?>px;
		position: relative;
	}
	body.header-is-sticky #page {
		padding-top: 97px;
	}
	body.tabby-sticky-header #page header.header {
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		z-index: 999;
		background-color: #fff;
	}
	body.header-is-sticky #page header.header {
		-webkit-box-shadow: 0 6px 12px -6px rgba(0,0,0,.1);
		-moz-box-shadow: 0 6px 12px -6px rgba(0,0,0,.1);
		box-shadow: 0 6px 12px -6px rgba(0,0,0,.1);
	}
	body.tabby-sticky-header.logged-in.admin-bar #page header.header {
		top: 32px;
	}

	header.header .header-top-bar {
		height: 52px;
		overflow: hidden;
		-webkit-transition: all .3s ease;
		-moz-transition: all .3s ease;
		-o-transition: all .3s ease;
		transition: all .3s ease;
	}
	body.header-is-sticky #page header.header .header-top-bar {
			height: 42px;
		}
	<?php $headerTopHeight = carbon_get_theme_option('tabby_header_height_top');  ?>
	<?php if (!empty($headerTopHeight)) :
		$percentage = ((20*(int)$headerTopHeight)/100);
		?>
		header.header .header-top-bar{
			height: <?php echo (int)$headerTopHeight?>px;
		}
		body.header-is-sticky #page header.header .header-top-bar {
			height: <?php echo (int)$headerTopHeight - $percentage ;?>px;
		}
		body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
			height: <?php echo (int)$headerTopHeight - $percentage ;?>px;
			line-height: <?php echo (int)$headerTopHeight - $percentage ;?>px;
		}
	<?php endif; ?>


	<?php if (carbon_get_theme_option('tabby_header_cta_btn_style') =='oval') :?>
	<?php $headerTopHeight = carbon_get_theme_option('tabby_header_height_top');  ?>
	<?php if (!empty($headerTopHeight)) :
	$percentage = ((40*(int)$headerTopHeight)/100);
	?>
		body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
			font-size: 15px;
			height: <?php echo (int)$headerTopHeight - $percentage ;?>px;
			line-height: <?php echo (int)$headerTopHeight - $percentage ;?>px;
		}
	<?php else: ?>
		body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
			font-size: 15px;
			height: 38px;
			line-height: 38px;
		}
	<?php endif; ?>

	<?php else: ?>
	<?php $headerTopHeight = carbon_get_theme_option('tabby_header_height_top');  ?>
	<?php if (!empty($headerTopHeight)) :
		$percentage = ((20*(int)$headerTopHeight)/100);
		?>
		body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
			font-size: 15px;
			height: <?php echo (int)$headerTopHeight - $percentage ;?>px;
			line-height: <?php echo (int)$headerTopHeight - $percentage ;?>px;
		}
	<?php else: ?>
		body.header-is-sticky #page header.header .header-top-bar .tabby-top-menu ul li.header-top-btn a {
			font-size: 15px;
			height: 42px;
			line-height: 42px;
		}
	<?php endif; ?>

	<?php endif; ?>



	header.header .header-main {
		height: 68px;
		-webkit-transition: all .3s ease;
		-moz-transition: all .3s ease;
		-o-transition: all .3s ease;
		transition: all .3s ease;
	}
	body.header-is-sticky #page header.header .header-main {
		padding: 12px 0;
		height: 55px;
	}
	<?php if (!empty($headerMainHeight = carbon_get_theme_option('tabby_header_height_main'))) : ?>
	header.header .header-main {
		height: <?php echo (int)$headerMainHeight; ?>px;
	}
	<?php endif; ?>
	<?php if (!empty($headerMainHeight = carbon_get_theme_option('tabby_header_height_main') ? carbon_get_theme_option('tabby_header_height_main') : "68")) :
		$percentage = ((20*(int)$headerMainHeight)/100);
		?>
	body.header-is-sticky #page header.header .header-main {
		height: <?php echo (int)$headerMainHeight - $percentage ?>px;
	}
	<?php endif; ?>
	body.header-is-sticky #page header.header .header-main .logo-wrap a img {
		max-width: 180px;
	}
	body.header-is-sticky #page header.header .header-main ul.menu>li a {
		font-size: .9rem;
	}
	body.header-is-sticky #page header.header .header-main ul.menu>li.menu-item-has-children>ul.sub-menu {
		padding-top: 16px;
	}
	body.tabby-sticky-header.tabby-header-layout-3 #page header.header,
	body.tabby-sticky-header.tabby-header-layout-4 #page header.header{
		background: none;
	}
	body.tabby-sticky-header.tabby-header-layout-3 #page,
	body.tabby-sticky-header.tabby-header-layout-4 #page{
		padding-top: 48px;
	}
	body.tabby-sticky-header.tabby-header-layout-3 #page header.header .header-main,
	body.tabby-sticky-header.tabby-header-layout-4 #page header.header .header-main {
		position: relative;
	}
}
<?php endif; ?>
<?php if (!empty($mobileMenuLayout) == 'tabbar_top') : ?>
	@media(max-width: <?php echo $breakpoint; ?>px){
		body.tabby-sticky-header.tabby-tabbar-top header.header{
			display: none !important;
		}
		body.tabby-sticky-header.tabby-tabbar-top #page{
			padding-top: 0;
		}
	}
<?php endif;?>

<?php if (!empty($headerMainHeight = carbon_get_theme_option('tabby_header_height_main'))) : ?>
	header.header .header-main {
	height: <?php echo (int)$headerMainHeight; ?>px;
}
<?php endif; ?>
<?php if (!empty($headerCtaButtonColor = carbon_get_theme_option('tabby_header_settings_cta_text_color')) && !empty($headerCtaButtonBg = carbon_get_theme_option('tabby_header_settings_cta_btn_bg_color'))) : ?>
	header.header .tabby-top-menu ul li.header-top-btn a{
	<?php if (!empty($headerCtaButtonBg)) :?>
	background: <?php  echo $headerCtaButtonBg; ?>;
	<?php endif; ?>
	<?php if (!empty($headerCtaButtonColor)) :?>
	color: <?php echo $headerCtaButtonColor;?>;
	<?php endif; ?>
}
<?php endif; ?>
<?php if (!empty($headerCtaButtonTextHoverColor = carbon_get_theme_option('tabby_header_settings_cta_btn_text_hover_color')) && !empty($headerCtaButtonBgHover = carbon_get_theme_option('tabby_header_settings_cta_btn_bg_hover_color'))) : ?>
	header.header .tabby-top-menu ul li.header-top-btn a:hover{
	<?php if (!empty($headerCtaButtonBgHover)) :?>
	background: <?php  echo $headerCtaButtonBgHover; ?>;
	<?php endif; ?>
	<?php if (!empty($headerCtaButtonTextHoverColor)) :?>
	color: <?php echo $headerCtaButtonTextHoverColor;?>;
	<?php endif; ?>
}
<?php endif; ?>
<?php if (!empty($headerTopHeight = carbon_get_theme_option('tabby_header_height_top'))) :?>

	<?php if (carbon_get_theme_option('tabby_header_cta_btn_style') =='oval') :?>
	header.header .tabby-top-menu ul li.header-top-btn a{
		height: 52px;
		line-height: 52px;
		display: flex;
		align-items: center;
	}
	<?php else: ?>
	header.header .tabby-top-menu ul li.header-top-btn a{
		height: <?php echo (int)$headerTopHeight;?>px;
		line-height: <?php echo (int)$headerTopHeight;?>px;
		display: flex;
		align-items: center;
	}
	<?php endif; ?>


<?php else : ?>
	<?php if (carbon_get_theme_option('tabby_header_cta_btn_style') =='oval') : ?>
		header.header .tabby-top-menu ul li.header-top-btn a{
			height: 44px;
			line-height: 44px;
			display: flex;
			align-items: center;
		}
    <?php else: ?>
		header.header .tabby-top-menu ul li.header-top-btn a{
			height: 52px;
			line-height: 52px;
			display: flex;
			align-items: center;
		}
    <?php endif; ?>
<?php endif; ?>
<?php if (!empty($headerPageHeight = carbon_get_theme_option('tabby_header_height_page_header'))) :?>
	.banner{
	min-height: <?php echo (int)$headerPageHeight;?>px;
	<?php if (!empty($isSticky)) : ?>
	margin-top: <?php echo (int)$headerTopHeight + (int)carbon_get_theme_option('tabby_header_height_main') - 120; ?>px;
	<?php endif; ?>
}
<?php endif; ?>
<?php if (!empty($mainNavBgColor = carbon_get_theme_option('tabby_header_main_desktop_bg'))) :?>
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li{
	background-color: <?php echo $mainNavBgColor;?>;
}
<?php endif; ?>
<?php if (!empty($mainNavBgHover= carbon_get_theme_option('tabby_header_main_desktop_bg_hover')) && !empty($mainNavTextHover = carbon_get_theme_option('tabby_header_main_desktop_text_hover'))) :?>
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li a:hover,
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li ul.sub-menu li a:hover{
	background-color: <?php echo $mainNavBgHover;?>;
	color: <?php echo $mainNavTextHover;?>;
}
<?php endif; ?>
<?php if (!empty($mainNavTextColor = carbon_get_theme_option('tabby_header_main_desktop_text_color'))) :?>
	header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li a{
	color: <?php echo $mainNavTextColor;?>;
}
<?php endif; ?>

<?php if (is_front_page()  && (!empty(carbon_get_theme_option('tabby_header_height_main')) || !empty(carbon_get_theme_option('tabby_header_height_top')))) :
	$topHeaderHeight = (int)carbon_get_theme_option('tabby_header_height_top') ? (int)carbon_get_theme_option('tabby_header_height_top') : '52';
	$mainHeaderHeight = (int)carbon_get_theme_option('tabby_header_height_main') ? (int)carbon_get_theme_option('tabby_header_height_main') : "68";
	?>
	.hero-wrapper.ugb-container.ugb-container--height-full {
		min-height: calc(100vh - <?php echo $mainHeaderHeight + $topHeaderHeight .'px'; ?>);
	}
	<?php if (!empty($isSticky) == 'yes') : ?>
		@media(min-width: 992px ){
			.home .site .site-content#content{
				margin-top: <?php echo ($mainHeaderHeight + $topHeaderHeight - $topHeaderHeight); ?>px;
			}
		}
	<?php endif; ?>
<?php elseif (is_front_page()) :
	$topHeaderHeight = (int)carbon_get_theme_option('tabby_header_height_top') ? (int)carbon_get_theme_option('tabby_header_height_top') : '52';
	$mainHeaderHeight = (int)carbon_get_theme_option('tabby_header_height_main') ? (int)carbon_get_theme_option('tabby_header_height_main') : "68";
	?>
	.hero-wrapper.ugb-container.ugb-container--height-full {
		min-height: calc(100vh - <?php echo $mainHeaderHeight + $topHeaderHeight .'px'; ?>);
	}
	<?php if (!empty($isSticky) == 'yes') : ?>
	@media(min-width: 992px ){
		.home .site .site-content#content{
			margin-top: <?php echo ($mainHeaderHeight + $topHeaderHeight - $topHeaderHeight); ?>px;
		}
	}
	<?php endif; ?>
<?php else :
	$topHeaderHeight = (int)carbon_get_theme_option('tabby_header_height_top') ? (int)carbon_get_theme_option('tabby_header_height_top') : '52';
	$mainHeaderHeight = (int)carbon_get_theme_option('tabby_header_height_main') ? (int)carbon_get_theme_option('tabby_header_height_main') : "68";
	?>
	<?php if (!empty($isSticky) == 'yes') : ?>
	@media(min-width: 992px ){
		.banner{
			margin-top: <?php echo ($mainHeaderHeight + $topHeaderHeight - $topHeaderHeight); ?>px;
		}
	}
	<?php endif; ?>
<?php endif; ?>

@media(max-width: 991px) {
	<?php if (!empty($headerManiMobileHeight = carbon_get_theme_option('tabby_header_height_main_mobile'))) : ?>
		header.header .header-main{
			height:<?php echo (int)$headerManiMobileHeight; ?>px;
			padding: 0;
		}
	<?php endif;?>
	<?php if (!empty($headerBannerMobileHeight = carbon_get_theme_option('tabby_header_height_page_header_mobile'))) : ?>
		.banner{
			min-height:<?php echo (int)$headerBannerMobileHeight; ?>px;
		}
	<?php endif;?>
}
<?php if (!empty($btnRectangelStyle = carbon_get_theme_option('tabby_header_cta_btn_style'))) : ?>
	header.header .tabby-top-menu ul li.header-top-btn a{
		<?php if (!empty($ctaBtnBg = carbon_get_theme_option('tabby_header_cta_btn_bg_color'))) :?>
		  background:<?php echo $ctaBtnBg;?>;
		<?php endif;?>
		  transition: 0.5s;
		<?php if (!empty($ctaBtncolor = carbon_get_theme_option('tabby_header_cta_btn_bg_text_color'))) :?>
		  color: <?php echo $ctaBtncolor;?>;
		<?php endif;?>
		<?php if (carbon_get_theme_option('tabby_header_cta_btn_style') =='rectangle') :?>
			<?php if ($ctaBorderBottomStyle = carbon_get_theme_option('tabby_header_cta_btn_border_bottom') == 'yes') :?>
				<?php if (!empty($ctaBorderBottomStyle)) :?>
					<?php if (!empty($ctaBorderBottomColor = carbon_get_theme_option('tabby_header_cta_btn_border_btn_color'))) :?>
			border-bottom-color: <?php echo $ctaBorderBottomColor;?>;
					<?php endif;?>
			padding: 0 18px ;
					<?php if (!empty($ctaBorderBottomWidth = carbon_get_theme_option('tabby_header_cta_btn_border_btn_height'))) :?>
			border-bottom-width: <?php echo (int)$ctaBorderBottomWidth;?>px;
					<?php else :?>
			border-bottom-width: 2px;
					<?php endif;?>
				<?php endif;?>
			<?php endif;?>
		<?php endif;?>
		<?php if (carbon_get_theme_option('tabby_header_cta_btn_style') =='oval') :?>

		<?php $borderColor = carbon_get_theme_option('tabby_header_cta_btn_border_outside_btn_color'); ?>
		border-bottom-color: <?php echo $borderColor ? $borderColor : 'inherit' ;?>;
		<?php if ($ctaBorderOutsideStyle = carbon_get_theme_option('tabby_header_cta_btn_border_outside') == 'yes') :?>
		<?php if (!empty($ctaBorderOutsideStyle)) :?>
		<?php if (!empty($borderColor)) :?>
		border-color: <?php echo $borderColor;?>;
		<?php endif;?>
		<?php if(!empty($ctaBorderOutsideWidth = carbon_get_theme_option('tabby_header_cta_btn_border_outside_width'))):?>
		border-width: <?php echo (int)$ctaBorderOutsideWidth;?>px;
		<?php endif;?>
		border-style: solid;
		border-radius: 50px;
		<?php endif;?>
		<?php else :?>
		border-radius: 50px;
		border: none;
		line-height: 20px;
		<?php endif;?>
		<?php endif;?>
	  }
<?php endif;?>
<?php if (!empty($btnRectangelStyle = carbon_get_theme_option('tabby_header_cta_btn_style'))) :?>
	header.header .tabby-top-menu ul li.header-top-btn a:hover{
		<?php if (!empty($ctaBtnBgHover = carbon_get_theme_option('tabby_header_cta_btn_bg_hover_color'))) :?>
		background:<?php echo $ctaBtnBgHover;?>;
		<?php endif;?>
		<?php if (!empty($ctaBtncolorHover = carbon_get_theme_option('tabby_header_cta_btn_bg_text_hover_color'))) :?>
		color: <?php echo $ctaBtncolorHover;?>;
		<?php endif;?>

		<?php if (carbon_get_theme_option('tabby_header_cta_btn_style') =='oval') :?>
			<?php if ($ctaBorderOutsideStyle = carbon_get_theme_option('tabby_header_cta_btn_border_outside') == 'yes') :?>
				<?php if (!empty($ctaBorderOutsideStyle)) :?>
					<?php if (!empty($ctaBorderOutsideHoverColor = carbon_get_theme_option('tabby_header_cta_btn_border_outside_btn_hover_color'))) :?>
			border-color: <?php echo $ctaBorderOutsideHoverColor;?>;
			border-bottom-color: <?php echo $ctaBorderOutsideHoverColor;?>;
					<?php endif;?>
				<?php endif;?>
			<?php endif;?>
		<?php else: ?>
	<?php if ($ctaBorderBottomStyle = carbon_get_theme_option('tabby_header_cta_btn_border_bottom') == 'yes') :?>
	<?php if (!empty($ctaBorderBottomStyle)) :?>
	<?php if (!empty($ctaBorderBottomHover = carbon_get_theme_option('tabby_header_cta_btn_border_btn_hover_color'))) :?>
		border-bottom-color: <?php echo $ctaBorderBottomHover;?>;
	<?php endif;?>
	<?php endif;?>
	<?php endif;?>
		<?php endif;?>
	}
<?php endif;?>
