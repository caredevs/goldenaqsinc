<?php
$socialLinkWidth = carbon_get_theme_option('tabby_social_icon_width');
$socialLinkHeight = carbon_get_theme_option('tabby_social_icon_height');
$socialLinkFontSize = carbon_get_theme_option('tabby_social_icon_font_size');
$socialLinkColor = carbon_get_theme_option('tabby_social_icon_color');
$socialLinkHoverColor = carbon_get_theme_option('tabby_social_icon_hover_color');
$socialLinkBgColor = carbon_get_theme_option('tabby_social_icon_bg_color');
$socialLinkBgHoverColor = carbon_get_theme_option('tabby_social_icon_bg_hover_color');

if(!empty($socialLinkWidth) || !empty($socialLinkHeight) || !empty($socialLinkFontSize) || !empty($socialLinkColor) || !empty($socialLinkBgColor)):
?>
ul.tabby-social-links li a{

	<?php if(!empty($socialLinkWidth)):?>
	width: <?php echo $socialLinkWidth;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkHeight)):?>
	height: <?php echo $socialLinkHeight;?>;
	line-height: <?php echo $socialLinkHeight;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkFontSize)):?>
	font-size: <?php echo $socialLinkFontSize;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkColor)):?>
	color: <?php echo $socialLinkColor;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkBgColor)):?>
	background-color: <?php echo $socialLinkBgColor;?>;
	<?php endif;?>
}
<?php endif;?>
<?php if(!empty($socialLinkBgHoverColor) || !empty($socialLinkHoverColor)): ?>
ul.tabby-social-links li a:hover{
	<?php if(!empty($socialLinkHoverColor)):?>
	color: <?php echo $socialLinkHoverColor;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkBgHoverColor)):?>
	background-color: <?php echo $socialLinkBgHoverColor;?>;
	<?php endif;?>
}
<?php endif;?>
<?php if(carbon_get_theme_option('tabby_sticky_header') == 'yes'): ?>
body.header-is-sticky #page header.header .header-top-bar ul.tabby-social-links li a{
<?php if(!empty($socialLinkWidth)):?>
	width: <?php echo (rtrim($socialLinkWidth,'px') - (rtrim($socialLinkWidth,'px') * 0.2)).'px';?>;
<?php endif;?>
<?php if(!empty($socialLinkHeight)):?>
	height: <?php echo (rtrim($socialLinkHeight,'px') - (rtrim($socialLinkHeight,'px') * 0.2)).'px';?>;
	line-height: <?php echo (rtrim($socialLinkHeight,'px') - (rtrim($socialLinkHeight,'px') * 0.2)).'px';?>;
<?php endif;?>
<?php if(!empty($socialLinkFontSize)):?>
	font-size: <?php echo (rtrim($socialLinkFontSize,'px') - (rtrim($socialLinkFontSize,'px') * 0.2)).'px';?>;
<?php endif;?>
}
<?php endif; ?>
