<?php if (!empty($bodyFontColor = carbon_get_theme_option('tabby_body_font_color'))) : ?>
	body,p,ul,ol,form input,form textarea,form select,form input[type="radio"], form input[type="checkbox"],blockquote,.fl-rich-text p{
	color : <?php echo $bodyFontColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyLinkColor = carbon_get_theme_option('tabby_link_color'))) : ?>
	a, .fl-builder-content .fl-button-wrap .fl-button{
		color: <?php echo $tabbyLinkColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyLinkHoverColor = carbon_get_theme_option('tabby_link_hover_color'))) : ?>
	a:hover, .fl-builder-content .fl-button-wrap .fl-button:hover,.fl-button-wrap a:focus{
		color: <?php echo $tabbyLinkHoverColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyPrimaryColor = carbon_get_theme_option('tabby_primary_color'))) : ?>
	.fl-menu > .menu > li > a,.fl-menu > .menu > li > .fl-has-submenu-container > a,.primary-heading .fl-module-content .fl-heading,.fl-module h1[class^="fl"][class$="title"],.fl-module h2[class^="fl"][class$="title"],.fl-module h3[class^="fl"][class$="title"],
	.fl-module h4[class^="fl"][class$="title"].fl-module h5[class^="fl"][class$="title"],.fl-module h6[class^="fl"][class$="title"],.fl-module h1,.fl-module h2,.fl-module h3,.fl-module h4,.fl-module h5,.fl-module h6,.text-primary .fl-rich-text p,.primary-txt,.text-primary .fl-module-content div[class^="fl-post"][class$="content"] p,.text-primary .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-primary .fl-module-content div[class^="fl-"][class$="text"] p,h1,h2,h3,h4,h5,h6,header.header.header-layout-3 .header-main .main-nav ul li.main-menu-btn a, header.header.header-layout-4 .header-main .main-nav ul li.main-menu-btn a,.button-primary-outline,  .hip-staff-block-wrapper.layout-grid .hip-staff-block-staff .staff-read-more a.button-primary, .blog-posts-wrapper .post-content>.read-more a,body a.primary-txt-hover:hover, body button.primary-txt-hover:hover{
		color: <?php echo $tabbyPrimaryColor; ?>
	}
.primary-background,.primary-bg .fl-row-content-wrap,.button-primary,.fl-builder-content .primary-btn .fl-module-content .fl-button-wrap a.fl-button,.primary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"],.primary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a,.fl-builder-content div.primary-btn .fl-module-content form button[type="submit"],header.header .header-main ul.menu li.main-menu-btn a,.banner:before,.tabby-block-btn.primary a,.button-primary-outline:hover, .hip-review-single-wrapper .quote-icon span, footer.site-footer .top-footer, .archive-pagination nav.navigation.pagination .nav-links span,body.header-is-sticky.tabby-header-layout-4 header .header-main, body.header-is-sticky.tabby-header-layout-3 header .header-main,body.tabby-hamburger-menu.tabby-sticky-header.header-is-sticky header, header.header .header-main .call-now-btn a{
	background-color: <?php echo $tabbyPrimaryColor; ?>;
}
.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit{
	background-color: <?php echo $tabbyPrimaryColor; ?>!important;
}
.button-primary-outline{
	border-color:<?php echo $tabbyPrimaryColor; ?>
}
.wp-block-hvc-video-block-video-from-collection.video-block .pps-video-wrap .pps-video-preview-wrap a span.push-btn:after{
	border-left: 25px solid <?php echo $tabbyPrimaryColor; ?>;
}
<?php endif;?>
<?php if (!empty($tabbyPrimaryHighlightColor = carbon_get_theme_option('tabby_primary_highlight_color'))) : ?>
	.text-primary-highlight .fl-rich-text p, .primary-txt-highlight,.text-primary-highlight .fl-module-content div[class^="fl-post"][class$="content"] p,.text-primary-highlight .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-primary-highlight .fl-module-content div[class^="fl-"][class$="text"] p{
		color: <?php echo $tabbyPrimaryHighlightColor; ?>
	}
.primary-highlight-background, .primary-bg-highlight .fl-row-content-wrap, .button-primary:hover, .button-primary:focus, .fl-builder-content .primary-btn .fl-module-content .fl-button-wrap a.fl-button:hover, .primary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:hover,.primary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:hover,.fl-builder-content div.primary-btn .fl-module-content form button[type="submit"]:hover,.fl-builder-content .primary-btn .fl-module-content .fl-button-wrap a.fl-button:focus, .primary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:focus,.primary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:focus,.fl-builder-content div.primary-btn .fl-module-content form button[type="submit"]:focus,header.header .header-main ul.menu li.main-menu-btn a:hover,.tabby-block-btn.primary-highlight a, .tabby-block-btn.primary a:hover, .tabbar .tab div.tabbar-menu .search-bar input, header.header .header-main .call-now-btn:hover a{
	background-color: <?php echo $tabbyPrimaryHighlightColor; ?>;
}
.booking-now-form .wpforms-submit-container button.wpforms-submit:hover, .contact form.wpforms-form .wpforms-submit-container button.wpforms-submit:hover{
	background-color: <?php echo $tabbyPrimaryHighlightColor; ?>!important;
}
<?php endif;?>
<?php if (!empty($tabbySecondaryColor = carbon_get_theme_option('tabby_secondary_color'))) : ?>
	.secondary-heading .fl-module-content .fl-heading,.fl-module.secondary-heading h1[class^="fl"][class$="title"],.fl-module.secondary-heading h2[class^="fl"][class$="title"],.fl-module.secondary-heading h3[class^="fl"][class$="title"],.fl-module.secondary-heading h4[class^="fl"][class$="title"].fl-module.secondary-heading h5[class^="fl"][class$="title"],.fl-module.secondary-heading h6[class^="fl"][class$="title"],.text-secondary .fl-rich-text p, .secondary-txt, .text-secondary .fl-module-content div[class^="fl-post"][class$="content"] p,.text-secondary .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-secondary .fl-module-content div[class^="fl-"][class$="text"] p{
		color:<?php echo $tabbySecondaryColor; ?>
	}
.bg-secondary, .secondary-bg .fl-row-content-wrap, .button-secondary, .fl-builder-content .secondary-btn .fl-module-content .fl-button-wrap a.fl-button,.secondary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"],.secondary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a,.fl-builder-content div.secondary-btn .fl-module-content form button[type="submit"],.tabby-block-btn.secondary a, header.header .tabby-top-menu ul li.header-top-btn a, header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li{
	background-color:<?php echo $tabbySecondaryColor; ?>
}
<?php endif;?>
<?php if (!empty($tabbySecondaryHighlightColor = carbon_get_theme_option('tabby_secondary_highlight_color'))) : ?>
	.text-secondary-highlight .fl-rich-text p, .secondary-txt-highlight, .text-secondary-highlight .fl-module-content div[class^="fl-post"][class$="content"] p,.text-secondary-highlight .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-secondary-highlight .fl-module-content div[class^="fl-"][class$="text"] p{
		color:<?php echo $tabbySecondaryHighlightColor; ?>
	}
.bg-secondary-highlight, .secondary-bg-highlight .fl-row-content-wrap, .button-secondary:hover, .button-secondary:focus,.fl-builder-content .secondary-btn .fl-module-content .fl-button-wrap a.fl-button:hover, .secondary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:hover,.secondary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:hover,.fl-builder-content div.secondary-btn .fl-module-content form button[type="submit"]:hover,.fl-builder-content .secondary-btn .fl-module-content .fl-button-wrap a.fl-button:focus, .secondary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:focus,.secondary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:focus,.fl-builder-content div.secondary-btn .fl-module-content form button[type="submit"]:focus,.tabby-block-btn.secondary-highlight a, header.header .tabby-top-menu ul li.header-top-btn a:hover, header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li a:hover{
	background-color:<?php echo $tabbySecondaryHighlightColor; ?>
}
header.header .tabby-top-menu ul li.header-top-btn a{
	border-bottom: 2px solid <?php echo $tabbySecondaryHighlightColor; ?>;
}
<?php endif;?>
<?php if (!empty($tabbyTertiaryColor = carbon_get_theme_option('tabby_tertiary_color'))) : ?>
	.bg-tertiary,.tertiary-bg .fl-row-content-wrap, .tertiary-bg-post .fl-post-grid .fl-post-grid-post,.tertiary-bg-post .fl-post-feed .fl-post-feed-post .fl-post-feed-text,.tertiary-bg-post .fl-post-carousel .fl-post-carousel-post{
		background-color:<?php echo $tabbyTertiaryColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyTertiaryHighlightColor = carbon_get_theme_option('tabby_tertiary_highlight_color'))) : ?>
	.bg-tertiary-highlight,.tertiary-highlight-bg .fl-row-content-wrap, .tertiary-highlight-bg-post .fl-post-grid .fl-post-grid-post,.tertiary-highlight-bg-post .fl-post-feed .fl-post-feed-post .fl-post-feed-text,.tertiary-highlight-bg-post .fl-post-carousel .fl-post-carousel-post{
		background-color:<?php echo $tabbyTertiaryHighlightColor; ?>
	}
<?php endif;?>
<?php if (!empty($breadcrumbLinkColor = carbon_get_theme_option('tabby_breadcrumb_link_color'))) : ?>
	.breadcrumbs-wrap ul li a{
		color: <?php echo $breadcrumbLinkColor;?>;
	}
<?php endif;?>
<?php if (!empty($breadcrumbLinkHoverColor = carbon_get_theme_option('tabby_breadcrumb_link_hover_color'))) : ?>
	.breadcrumbs-wrap ul li a:hover,.breadcrumbs-wrap ul li .current{
		color: <?php echo $breadcrumbLinkHoverColor;?>;
	}
<?php endif;?>
<?php if (!empty($breadcrumbSepColor = carbon_get_theme_option('tabby_breadcrumb_separator_color'))) : ?>
	.breadcrumbs-wrap ul li i{
		color: <?php echo $breadcrumbSepColor;?>;
	}
<?php endif;?>
<?php if (!empty($breadcrumbBgColor = carbon_get_theme_option('tabby_breadcrumb_bg_color'))) : ?>
	.tabby-breadcrumb{
		background-color: <?php echo $breadcrumbBgColor;?>;
	}
<?php endif;?>