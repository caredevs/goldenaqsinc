<?php

if (!defined('GOLDENAQSINC_THEME_VERSION')) define('GOLDENAQSINC_THEME_VERSION', '1.0');

require_once (get_template_directory() . '/lib/ThemeOptions.php');
require_once (get_template_directory() . '/lib/ThemeOptions/GeneralSettings.php');
require_once (get_template_directory() . '/lib/ThemeOptions/BusinessSettings.php');
require_once (get_template_directory() . '/lib/ThemeOptions/HeaderSettings.php');
require_once (get_template_directory() . '/lib/ThemeOptions/FooterSettings.php');
require_once (get_template_directory() . '/lib/ThemeOptions/LayoutSettings.php');
require_once (get_template_directory() . '/lib/Breadcrumbs.php');
require_once (get_template_directory() . '/lib/GutenbergBlocks.php');
require_once (get_template_directory() . '/lib/CustomPostType.php');
//require_once (get_template_directory() . '/lib/Metaboxes/metaboxSettings.php');
//require_once (get_template_directory() . '/lib/Reviews/Reviews.php');

if (!class_exists('Goldenaqsinc_Theme_Setup'))
{
    class Goldenaqsinc_Theme_Setup {




        public function __construct() {
            add_action('wp_enqueue_scripts', array( $this, 'Goldenaqsinc_enqueue_scripts' ));
            add_action('after_setup_theme', array($this, 'Goldenaqsinc_after_theme_setup'));
			add_action('admin_enqueue_scripts',array($this, 'goldenaqsincAdminScripts'));
			new \Tabby\ThemeOptions();
        }
        public function Goldenaqsinc_enqueue_scripts() {


            /*
            * Enqueue css Files
            *
            */
			wp_register_style( 'line-awesome', 'https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css' );
			wp_enqueue_style('line-awesome');

            wp_enqueue_style('font_awesome', get_template_directory_uri() . '/assets/vendor/Font-Awesome/web-fonts-with-css/css/fontawesome-all.min.css', array() , GOLDENAQSINC_THEME_VERSION);

            wp_enqueue_style('slick', get_template_directory_uri() . '/assets/dist/css/slick.css', array() , GOLDENAQSINC_THEME_VERSION);

            wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/dist/css/slick-theme.css', array() , GOLDENAQSINC_THEME_VERSION);
            wp_enqueue_style('animate', get_template_directory_uri() . '/assets/dist/css/animate.min.css', array() , GOLDENAQSINC_THEME_VERSION);
            wp_enqueue_style('font-css', get_template_directory_uri() . '/assets/dist/css/font-css.css', array() , GOLDENAQSINC_THEME_VERSION);

            wp_enqueue_style('site_style', get_template_directory_uri() . '/assets/dist/css/site.css', array() , GOLDENAQSINC_THEME_VERSION);

            wp_enqueue_style( 'stylesheet', get_stylesheet_uri() );
            /*
             * Enqueue jQuery Files
             *
             */
            wp_enqueue_script('jQuery');

            wp_register_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(
                'jquery'
            ) , GOLDENAQSINC_THEME_VERSION, true);
            wp_enqueue_script('popper');

            wp_enqueue_script('bootstrap_min', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', array(
                'jquery'
            ) , GOLDENAQSINC_THEME_VERSION, true);

            wp_enqueue_script('slick', get_template_directory_uri() . '/assets/dist/js/slick.min.js', array(
                'jquery'
            ) , GOLDENAQSINC_THEME_VERSION, true);
            wp_enqueue_script('site_js', get_template_directory_uri() . '/assets/dist/js/site.js', array(
                'jquery'
            ) , GOLDENAQSINC_THEME_VERSION, true);
            wp_localize_script( 'site_js', 'siteInfo', array(
                'siteUrl' => site_url()
            ) );

        }

		/**
		 * can be moved into separate function or file
		 */

        public function Goldenaqsinc_after_theme_setup() {


			/**
			 * widget areas
			 */
			include get_template_directory() . '/inc/widgetAreas.php';
			/**
			 * Recent Post widget
			 */
			include get_template_directory() . '/inc/widgets/recent-post.php';
			/**
			 * Recent Post widget
			 */
			include get_template_directory() . '/inc/widgets/popular-categories.php';


			//include get_template_directory() . '/lib/GetenbergBlocks.php';
			require_once(get_template_directory() . '/lib/MemberRegistration/MemberRegistration.php');

			new \GoldenAqsInc\GutenbergBlocks();
			new \GoldenAqsInc\CustomPostType();

            /*
             * load text domain
             *
             */
            load_theme_textdomain('goldenaqsinc', get_template_directory() . '/languages');

            /*
             * theme supports
             *
             */

            add_theme_support( 'post-thumbnails', array( 'post','page','features','team_members','agents' ) );
            add_theme_support('title-tag');

            /*
            * register nav
            *
            */

			// This theme uses wp_nav_menu() in one location.
			register_nav_menus([
				'main'		=>  esc_html__('Primary menu','goldenaqsinc'),
				'secondary'		=>  esc_html__('Secondary header menu','goldenaqsinc'),
				'footer'		=> esc_html__('Footer Menu','goldenaqsinc'),
				'mobile'	=> esc_html__('Mobile Dropdown Menu','goldenaqsinc'),
				'hero'		=> esc_html__('Hero Dropdown Menu','goldenaqsinc')
			]);
        }
		public function goldenaqsincAdminScripts()
		{
			wp_enqueue_style('tabby-admin-main',get_template_directory_uri().'/assets/dist/css/admin.css','',GOLDENAQSINC_THEME_VERSION);
		}


    }
}
if (class_exists('Goldenaqsinc_Theme_Setup'))
{
    $GLOBALS['goldenaqsinc'] = new Goldenaqsinc_Theme_Setup();
}

	add_shortcode('goldenaqsinc-breadcrumbs', function ($atts) {
		global $post;
		if ($post) {
			$breadcrumbs = new \GoldenAqsInc\Breadcrumbs($post);
			return $breadcrumbs->render();
		}
		return false;
	});