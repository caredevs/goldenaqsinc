<section class="about-us">
	<div class="container">
		<div class="row align-items-center my-5">
			<?php
			$imgId = $fields['gaqsinc_section_welcome_image'];
			$beforeTitle = $fields['gaqsinc_section_welcome_before_title'];
			$title = $fields['gaqsinc_section_welcome_title'];
			$content = $fields['gaqsinc_section_welcome_content'];
			$readMoreText = $fields['gaqsinc_section_welcome_readmore_text'];
			$readMoreLink = $fields['gaqsinc_section_welcome_readmore_link'];
			$hide = $fields['gaqsinc_section_welcome_readmore_link_hide'];
			?>
			<div class="col-12 col-md-6">
				<div class="about-thumb">
					<?php $url = wp_get_attachment_image_url( $imgId,'full' ); ?>
					<img src="<?php echo $url; ?>" class="img-fluid" alt="about-img">
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="about-info">
					<?php if(!empty($beforeTitle)): ?>
					<span class="gaqsinc-primary-text pb-5 d-block"><?php _e($beforeTitle,'goldenaqsinc'); ?></span>
					<?php endif; ?>
					<?php if(!empty($title)): ?>
					<h1 class="mb-5"><?php _e($title,'goldenaqsinc'); ?></h1>
					<?php endif; ?>
					<?php if(!empty($content)): ?>
					<p class="mb-5"><?php _e($content, 'goldenaqsinc'); ?></p>
					<?php endif; ?>
					<?php if(empty($hide)):?>
					<a href="<?php echo $readMoreLink; ?>" class="gaqsinc-primary-text font-weight-bold"><?php $readMoreText ? _e($readMoreText,'goldenaqsinc') : esc_html('Read More','goldenaqsinc'); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>