<section class="team-member">
	<div class="container">
		<div class="row py-5">
			<div class="col-12 col-md-4 offset-md-4">
				<div class="section-heading text-center">
					<span class="gaqsinc-primary-text">Expert </span>
					<h1>Meet our best Team members</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="members">
					<div class="member-single text-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/1-team.jpg" alt="team-member" class="img-fluid">
						<h4 class="pt-2">Crestive eve</h4>
						<span class="gaqsinc-primary-text">Assistant</span>
						<ul class="member-social-meida">
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-twitter"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-linkedin-in"></i></a>
							</li>
						</ul>
					</div>
					<div class="member-single text-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/2-team.jpg" alt="team-member" class="img-fluid">
						<h4 class="pt-2">Md. Farruk Islam</h4>
						<span class="gaqsinc-primary-text">Assistant</span>
						<ul class="member-social-meida">
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-twitter"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-linkedin-in"></i></a>
							</li>
						</ul>
					</div>
					<div class="member-single text-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/3-team.jpg" alt="team-member" class="img-fluid">
						<h4 class="pt-2">Jodien morch</h4>
						<span class="gaqsinc-primary-text">Assistant</span>
						<ul class="member-social-meida">
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-twitter"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-linkedin-in"></i></a>
							</li>
						</ul>
					</div>
					<div class="member-single text-center">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/1-team.jpg" alt="team-member" class="img-fluid">
						<h4 class="pt-2">Md. Farruk Islam</h4>
						<span class="gaqsinc-primary-text">Assistant</span>
						<ul class="member-social-meida">
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-facebook-f"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-twitter"></i></a>
							</li>
							<li class="d-inline-block">
								<a href="#"><i class="fab fa-linkedin-in"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>