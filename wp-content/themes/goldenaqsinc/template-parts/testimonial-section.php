<section id="about-us" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/bg-10.jpg)">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-sm-7">
				<div class="about-content">
					<h1>Client Testimonials</h1>
					<div class="client-testimonials pt-4">
						<div class="testimonial-item">
							<h3>Md. Farruk Islam</h3>
							<span class="pb-3 d-block">Co-founder & CEO, Bench </span>
							<p>"Zirtual is simple, accessible and effective; working with Jessica has been great. She was quick to pick up the tools Bench uses and integrates herself in the way we work."</p>
						</div>
						<div class="testimonial-item">
							<h3>Md. Farruk Islam</h3>
							<span class="pb-3 d-block">Co-founder & CEO, Bench </span>
							<p>"Zirtual is simple, accessible and effective; working with Jessica has been great. She was quick to pick up the tools Bench uses and integrates herself in the way we work."</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>