<?php
/**
 * Default page
 * @package tabby
 * @since 1.0.0
 */
get_header();
get_template_part('template-parts/banner-layout-1');
?>
<div class="container page-with-sidebar-template">
	<div class="row py-4 py-md-5">
		<div class="col-12 col-md-7 col-lg-8">
			<div class="main-content">
				<?php
				if(have_posts()){
					while ( have_posts() ) : the_post();
						the_content();
					endwhile;
				}
				wp_reset_postdata();
				?>
			</div>
		</div>
		<div class="col-12 col-md-5 col-lg-4">
			<?php if ( is_active_sidebar('page_sidebar')) : ?>
				<?php dynamic_sidebar('page_sidebar'); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer();?>
