<?php
/**
 * The header file
 * @package WordPress
 * @subpackage orange-theme
 * @version 1.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="header">
	<div class="header-top-bar primary-highlight-background d-none d-lg-block">
		<div class="container h-100">
			<div class="row align-items-center h-100 py-2">
				<div class="col-auto">
					<div class="header-top-info">
						<p class="text-white mb-0">
							<a href="tel:<?php echo do_shortcode('[goldenaqsinc-business-phone-number]');?>">
								<i class="fas fa-phone mr-2"> </i>
								<?php echo do_shortcode('[goldenaqsinc-business-phone-number]');?>
							</a>
						</p>
					</div>
				</div>
				<div class="col ml-auto d-flex justify-content-end">
					<?php echo do_shortcode('[goldenaqsinc-business-social-links]');?>

					<div class="tabby-top-menu">
					    <ul class="secondary-nav">
					        <?php if(is_user_logged_in()): ?>
							<li><a href="<?php echo wp_logout_url( home_url().'/login' ); ?>">Logout</a></li>
							<?php else: ?>
							<li><a href="<?php echo home_url().'/login'; ?>">Login</a></li>
							<?php endif; ?>
							<?php if(!is_user_logged_in()): ?>
							<li><a href="<?php echo home_url().'/registration'; ?>">Registration</a></li>
							<?php endif; ?>
					    </ul>
					</div>

					<span class="select-language text-white ml-5">Select  language
						<span class="dropdown ml-3">
						  <button class="btn btn-secondary dropdown-toggle btn-sm bg-white text-dark border-0" type="button" id="dropdownMenuButton" data-toggle="dropdown">English</button>
						  <span class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="#">German</a>
						  </span>
						</span>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="header-main">
		<div class="container h-100">
			<div class="row align-items-center h-100">
				<div class="col-auto">
					<div class="logo">
						<a href="<?php get_bloginfo('url');?>">
							<?php echo do_shortcode('[goldenaqsinc-logo]');?>
						</a>
					</div>
				</div>
				<div class="col">
					<div class="main-nav text-right d-none d-lg-block">
						<?php
						if ( has_nav_menu( 'main' ) ) :
							wp_nav_menu( array(
								'menu'              => __( 'Primary Nav', 'goldenaqsinc'),
								'theme_location'    => 'main',
							));
						endif;
						?>
					</div>
					<div class="text-right call-now-btn d-block d-lg-none ">
						<?php if(!empty($tabbyMenuLayout = carbon_get_theme_option('tabby_menu_layout'))): ?>
							<?php if($tabbyMenuLayout == 'hamburger'): ?>
								<div class="tab hamburger-btn-wrap">
									<button>
										<p class="c-hamburger c-hamburger--htx"><span></span></p>
									</button>
								</div>
							<?php else: ?>
								<a href="tel:<?php echo do_shortcode('[goldenaqsinc-business-phone-number]');?>"><i class="fa fa-phone"></i><span class="d-none d-sm-inline-block ml-2">Call Now</span></a>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if(!empty($tabbyMenuLayout = carbon_get_theme_option('tabby_menu_layout'))): ?>
		<?php if($tabbyMenuLayout == 'hamburger'): ?>
			<div class="hamburger-menu-wrap-continer d-block d-lg-none">
				<?php get_template_part('template-parts/hamburger-menu'); ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</header>

<main>
