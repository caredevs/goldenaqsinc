<?php
/*Template Name: Front page */
get_header(); ?>
	<section class="hero">
		<div class="slider">
			<div class="slick-slide text-right" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/bg-3.jpg)">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 data-animation="fadeInDown" data-delay="0.4s">Norie Dariah</h1>
							<p data-animation="fadeInUp" data-delay="0.6s">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et, ipsam. Qui saepe <br>quisquam iste dicta, vel ea omnis, distinctio nulla veniam.</p>
							<a href="#" class="btn gaqsinc-btn-primary btn-lg" data-animation="fadeInUp" data-delay="0.8s">Contact Me</a>
							<a href="#" class="btn gaqsinc-btn-secondary btn-lg" data-animation="fadeInUp" data-delay="0.9s">About Us</a>
						</div>
					</div>
				</div>
			</div><!-- slick-slide -->

			<div class="slick-slide text-left" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/slider_2.jpg)">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 data-animation="fadeInDown" data-delay="0.4s">Personal Website Title Show in here.</h1>
							<p data-animation="fadeInUp" data-delay="0.6s">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et, ipsam. Qui saepe <br>quisquam iste dicta, vel ea omnis, distinctio nulla veniam.</p>
							<a href="#" class="btn gaqsinc-btn-primary btn-lg" data-animation="fadeInUp" data-delay="0.8s">Contact Me</a>
							<a href="#" class="btn gaqsinc-btn-secondary btn-lg" data-animation="fadeInUp" data-delay="0.9s">About Us</a>
						</div>
					</div>
				</div>
			</div><!-- slick-slide -->
		</div>
	</section>
	<section id="client" class="pt-5 pb-5">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 offset-md-3">
					<h1 class="client-logo-title text-center">GET SUBSCRIPTION STAFF WITH EXPERIENCE AT THESE GREAT COMPANIES</h1>
				</div>
			</div>
			<div class="row pt-3">
				<div class="col-12">
					<div class="client-logo-wrap">
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/google.png">
						</div>
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bench.png">
						</div>
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png">
						</div>
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/spotify.png">
						</div>
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/google.png">
						</div>
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bench.png">
						</div>
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png">
						</div>
						<div class="client-logo">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/spotify.png">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="about-us">
		<div class="container">
			<div class="row align-items-center my-5">
				<div class="col-12 col-md-6">
					<div class="about-thumb">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/img_abt1.jpg" class="img-fluid">
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="about-info">
						<span class="gaqsinc-primary-text pb-5 d-block">Welcome</span>
						<h1 class="mb-5">Do You Need a Personal Assistant?</h1>
						<p class="mb-5">This website template is your number one helper for creating that perfect site for your personal or corporate needs! Let your projects obtain the most attractive appearance, and provide the best user experience! Welcome to se the services.</p>
						<a href="#" class="gaqsinc-primary-text font-weight-bold">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="how-it-work pt-3 pb-5">
		<div class="container">
			<div class="row py-5">
				<div class="col">
					<div class="section-heading text-center">
						<h1>How It Works</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="work-process-item mb-5">
						<span>01</span>
						<h3><a href="#">Handling The Task</a></h3>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-2.png" class="img-fluid my-4" alt="icon">
						<span class="border-section"></span>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="work-process-item mb-5 active">
						<span>02</span>
						<h3><a href="#">Creating To-Do List</a></h3>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-3.png" class="img-fluid my-4" alt="icon">
						<span class="border-section"></span>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="work-process-item mb-5">
						<span>03</span>
						<h3><a href="#">Schedule a Meeting</a></h3>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-1.png" class="img-fluid my-4" alt="icon">
						<span class="border-section"></span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="features">
		<div class="row align-items-center">
			<div class="col-12 col-md-6">
				<div class="feature-thumb">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/post-9.jpg" alt="" class="img-fluid">
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="feature-info">
					<h3>The Main Features</h3>
					<p>A personal assistant helps with time and daily management, scheduling of meetings, correspondence, and note taking. The role of a personal assistant can be varied, such as answering phone calls, scheduling meetings, emailing, texts etc.</p>
					<a href="#" class="gaqsinc-primary-text">Read More</a>
				</div>
			</div>
		</div>
		<div class="row align-items-center" style="background: #e0e0e0;">
			<div class="col-12 col-md-6">
				<div class="feature-info">
					<h3>Get Your Time Back</h3>
					<ul class="list-unstyled mt-3">
						<li class="py-2"><i class="far fa-check-circle mr-3 gaqsinc-primary-text"></i>There is no limit on how much you dedicate.</li>
						<li class="py-2"><i class="far fa-check-circle mr-3 gaqsinc-primary-text"></i>I will learn the skills that are required for your business.</li>
						<li class="py-2"><i class="far fa-check-circle mr-3 gaqsinc-primary-text"></i>Prior to doing work we get updates.</li>
					</ul>
					<a href="#" class="gaqsinc-primary-text">Read More</a>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="feature-thumb">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/img_column2.jpg" alt="" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="resource">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center pt-5 pb-2">
					<div class="primary-heading mb-5">
						<h1>Our Services</h1>
					</div>
					<p>Call, email, or text and engage a human, not a computer. Your Virtual Assistant is here for you to provide exceptional service - the same service they'd want to experience themselves.</p>
				</div>
			</div>
			<div class="row pt-3">
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
			</div>
			<div class="row py-5">
				<div class="col">
					<div class="section-btn text-center">
						<a class="btn gaqsinc-btn-primary rounded-0 btn-lg text-white">See All Services</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="about-us" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/bg-10.jpg)">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-sm-7">
					<div class="about-content">
						<h1>Client Testimonials</h1>
						<div class="client-testimonials pt-4">
							<div class="testimonial-item">
								<h3>Md. Farruk Islam</h3>
								<span class="pb-3 d-block">Co-founder & CEO, Bench </span>
								<p>"Zirtual is simple, accessible and effective; working with Jessica has been great. She was quick to pick up the tools Bench uses and integrates herself in the way we work."</p>
							</div>
							<div class="testimonial-item">
								<h3>Md. Farruk Islam</h3>
								<span class="pb-3 d-block">Co-founder & CEO, Bench </span>
								<p>"Zirtual is simple, accessible and effective; working with Jessica has been great. She was quick to pick up the tools Bench uses and integrates herself in the way we work."</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="team-member">
		<div class="container">
			<div class="row py-5">
				<div class="col-12 col-md-4 offset-md-4">
					<div class="section-heading text-center">
						<span class="gaqsinc-primary-text">Expert </span>
						<h1>Meet our best Team members</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="members">
						<div class="member-single text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/1-team.jpg" alt="team-member" class="img-fluid">
							<h4 class="pt-2">Crestive eve</h4>
							<span class="gaqsinc-primary-text">Assistant</span>
							<ul class="member-social-meida">
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-twitter"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-linkedin-in"></i></a>
								</li>
							</ul>
						</div>
						<div class="member-single text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/2-team.jpg" alt="team-member" class="img-fluid">
							<h4 class="pt-2">Md. Farruk Islam</h4>
							<span class="gaqsinc-primary-text">Assistant</span>
							<ul class="member-social-meida">
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-twitter"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-linkedin-in"></i></a>
								</li>
							</ul>
						</div>
						<div class="member-single text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/3-team.jpg" alt="team-member" class="img-fluid">
							<h4 class="pt-2">Jodien morch</h4>
							<span class="gaqsinc-primary-text">Assistant</span>
							<ul class="member-social-meida">
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-twitter"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-linkedin-in"></i></a>
								</li>
							</ul>
						</div>
						<div class="member-single text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/1-team.jpg" alt="team-member" class="img-fluid">
							<h4 class="pt-2">Md. Farruk Islam</h4>
							<span class="gaqsinc-primary-text">Assistant</span>
							<ul class="member-social-meida">
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-twitter"></i></a>
								</li>
								<li class="d-inline-block">
									<a href="#"><i class="fab fa-linkedin-in"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="register-section pt-5 pb-5" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/a11.jpg)">
		<div class="container h-100">
			<div class="row h-100 d-flex align-items-center">
				<div class="col-12 col-lg-8">
					<div class="register-info pb-5 d-block">
						<span class="gaqsinc-primary-text d-block mb-3">Contact</span>
						<h1 class="gaqsinc-secondary-light-text">Tell us what you need so you can <span>meet your virtual assistant.</span></h1>
					</div>
					<div class="register-form">
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<select name="business" class="form-control rounded-0" id="select-business" >
										<option value="For my business">For my business</option>
										<option value="For my business">For my home</option>
									</select>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="text" name="company" placeholder="Company" class="form-control rounded-0">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="text" name="firstname" placeholder="First name" class="form-control rounded-0">
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="text" name="lastname" placeholder="Last name" class="form-control rounded-0">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="email" name="email" placeholder="Email" class="form-control rounded-0">
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="submit" name="register" value="Register">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>