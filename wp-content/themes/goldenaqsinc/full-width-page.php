<?php
/**
 * Template Name: Full Width Page
 * Template Post Type: lp,page
 * @package Goldenaqsinc
 * @since 1.0.0
 */
get_header();
get_template_part('template-parts/banner-layout-1')
?>
<div class="full-width-page-template">
	<?php
	if(have_posts()){
		while ( have_posts() ) : the_post();
			the_content();
		endwhile;
	}
	wp_reset_postdata();
	?>
</div>
<?php get_footer();?>
