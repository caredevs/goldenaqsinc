<header id="header">
    <div class="slider">
        <div class="slick-slide text-right" style="background-image: url(https://bit.ly/2jY4fwy)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 data-animation="fadeInDown" data-delay="0.4s">Norie Dariah</h1>
                        <p data-animation="fadeInUp" data-delay="0.6s">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et, ipsam. Qui saepe <br>quisquam iste dicta, vel ea omnis, distinctio nulla veniam.</p>
                        <a href="#" class="btn btn-danger btn-lg" data-animation="fadeInUp" data-delay="0.8s">Contact Me</a>
                        <a href="#" class="btn btn-primary btn-lg" data-animation="fadeInUp" data-delay="0.9s">About Us</a>
                    </div>
                </div>
            </div>
        </div><!-- slick-slide -->

        <div class="slick-slide text-left" style="background-image: url(https://bit.ly/2Irdksl)">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 data-animation="fadeInDown" data-delay="0.4s">Personal Website Title Show in here.</h1>
                        <p data-animation="fadeInUp" data-delay="0.6s">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et, ipsam. Qui saepe <br>quisquam iste dicta, vel ea omnis, distinctio nulla veniam.</p>
                        <a href="#" class="btn btn-danger btn-lg" data-animation="fadeInUp" data-delay="0.8s">Contact Me</a>
                        <a href="#" class="btn btn-primary btn-lg" data-animation="fadeInUp" data-delay="0.9s">About Us</a>
                    </div>
                </div>
            </div>
        </div><!-- slick-slide -->
    </div>
</header>