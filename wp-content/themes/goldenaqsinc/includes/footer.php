<footer id="footer" class="pt-2 pb-2">
    <div class="container">
        <div class="row">
            <div  class="col">
                <div class="copyright">
                    <p>© 2018 - norie-dariah, Inc. All rights reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>