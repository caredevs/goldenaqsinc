<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Norie Dariah</title>
    <!-- Fontawesome -->
    <link rel="stylesheet" href="assets/vendor/Font-Awesome/web-fonts-with-css/css/fontawesome-all.min.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/vendor/OwlCarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/vendor/OwlCarousel2/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/dist/css/slick.css">
    <link rel="stylesheet" href="assets/dist/css/slick-theme.css">
    <link rel="stylesheet" href="assets/dist/css/animate.min.css">
    <link rel="stylesheet" href="assets/dist/css/font-css.css">
    <!-- Main css -->
    <link rel="stylesheet" href="assets/dist/css/site.css">
</head>
<body>