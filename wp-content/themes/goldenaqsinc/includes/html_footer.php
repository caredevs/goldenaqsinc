<!-- SCRIPTS -->
<!-- Jquery -->
<script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<!-- Popper(required for bootstrap 4)-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<!-- Bootstrap -->
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Owl carousel 2-->
<script src="assets/vendor/OwlCarousel2/dist/owl.carousel.min.js"></script>

<script src="assets/dist/js/slick.min.js"></script>
<!-- Site script -->
<script src="assets/dist/js/site.js"></script>
</body>
</html>