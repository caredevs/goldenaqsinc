<?php
/**
 * The header file
 * @package WordPress
 * @subpackage orange-theme
 * @version 1.0
 */

?>
</main>
<footer id="colophon" class="site-footer">

	<div class="top-footer layout-3">
		<div class="container">
			<div class="row">
				<div class="col">
					<?php if ( is_active_sidebar( 'above_footer' ) ) : ?>
						<?php dynamic_sidebar( 'above_footer' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-3 footer-col-1">
					<?php if ( is_active_sidebar('footer_col_1')) : ?>
						<?php dynamic_sidebar('footer_col_1'); ?>
					<?php endif; ?>
				</div>
				<div class="col-12 col-sm-6 col-md-3 footer-col-2">
					<?php if ( is_active_sidebar('footer_col_2')) : ?>
						<?php dynamic_sidebar('footer_col_2'); ?>
					<?php endif; ?>
				</div>
				<div class="col-12 col-sm-6 col-md-3 footer-col-3">
					<?php if ( is_active_sidebar( 'footer_col_3')) : ?>
						<?php dynamic_sidebar('footer_col_3'); ?>
					<?php endif; ?>
				</div>
				<div class="col-12 col-sm-6 col-md-3 footer-col-4">
					<?php if ( is_active_sidebar('footer_col_4' )) : ?>
						<?php dynamic_sidebar('footer_col_4'); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col">
					<?php if ( is_active_sidebar( 'sub_footer' ) ) : ?>
						<?php dynamic_sidebar( 'sub_footer' ); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</footer><!-- #colophon -->
<?php wp_footer(); ?>
</body>
</html>