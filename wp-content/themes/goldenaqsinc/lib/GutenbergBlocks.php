<?php
namespace GoldenAqsInc;
use Carbon_Fields\Block;
use Carbon_Fields\Field;
use League\Flysystem\Exception;

class GutenbergBlocks{

	public function __construct()
	{
		$this->initBlocks();
	}
	public function initBlocks()
	{
		$this->welComeSection();
		$this->teamMemberSection();
		$this->testimonialSection();
	}
	public function welComeSection(){
		Block::make( __( 'Welcome Section' ))
			->add_fields( array(
				Field::make( 'separator', 'gaqsinc_welcome_block_title', __( 'Welcome Section' )),
				Field::make( 'image', 'gaqsinc_section_welcome_image', __( 'Welcome Image' )),
				Field::make( 'text', 'gaqsinc_section_welcome_before_title', __( 'Sub Text before Title' )),
				Field::make( 'text', 'gaqsinc_section_welcome_title', __( 'Title' )),
				Field::make( 'text', 'gaqsinc_section_welcome_content', __( 'Content' )),

				Field::make( 'text', 'gaqsinc_section_welcome_readmore_text', __( 'Read More Link Text' )),
				Field::make( 'text', 'gaqsinc_section_welcome_readmore_link', __( 'Read More Link Link' )),
				Field::make( 'checkbox', 'gaqsinc_section_welcome_readmore_link_hide', __( 'Hide Read More Link' )),
			))
			->set_category( 'goldenaqsinc-blocks', __( 'Golden AQS Inc Blocks' ))
			->set_render_callback( function ( $fields, $attributes, $inner_blocks ){
				ob_start();
				?>
				<?php
					include(get_template_directory().'/template-parts/welcome-section.php');?>
				<?php
				return ob_get_flush();
			} );
	}
	public function testimonialSection(){
		Block::make( __( 'Testimonial' ))
			->add_fields( array(
				Field::make( 'separator', 'gaqsinc_testimonial_block', __( 'Testimonial Section' )),
				Field::make( 'image', 'gaqsinc_section_bg_image', __( 'Section BG Image' )),
				Field::make( 'text', 'gaqsinc_section_testimonial_title', __( 'Title' )),
			))
			->set_category( 'goldenaqsinc-blocks', __( 'Golden AQS Inc Blocks' ))
			->set_render_callback( function ( $fields, $attributes, $inner_blocks ){
				ob_start();
				?>
				<?php
				include(get_template_directory().'/template-parts/testimonial-section.php');?>
				<?php
				return ob_get_flush();
			} );
	}
	public function teamMemberSection(){
		Block::make( __( 'Team Member Section' ))
			->add_fields( array(
				Field::make( 'separator', 'gaqsinc_team_member_block', __( 'Team Member Section' )),
				Field::make( 'text', 'gaqsinc_section_team_member_before_title', __( 'Sub Text before Title' )),
				Field::make( 'text', 'gaqsinc_section_team_member_title', __( 'Title' )),
			))
			->set_category( 'goldenaqsinc-blocks', __( 'Golden AQS Inc Blocks' ))
			->set_render_callback( function ( $fields, $attributes, $inner_blocks ){
				ob_start();
				?>
				<?php
					include(get_template_directory().'/template-parts/team-member-section.php');?>
				<?php
				return ob_get_flush();
			} );
	}

}