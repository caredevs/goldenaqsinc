<?php
namespace Tabby\ThemeOptions;
use Carbon_Fields\Field;

class BusinessSettings
{
	public $settingsFields;

	public function __construct()
	{
		$this->settingsFields = $this->renderSettingsFields();
		add_shortcode('goldenaqsinc-business-address',[$this,'renderAddress']);
		add_shortcode('goldenaqsinc-business-phone-number',[$this,'renderPhoneNumber']);
		add_shortcode('goldenaqsinc-business-phone-number-link',[$this,'renderPhoneNumberLink']);
		add_shortcode('goldenaqsinc-business-social-links',[$this,'renderSocialMediaLinks']);
	}
	public function renderSettingsFields()
	{
		return array_merge($this->businessInfoSettings(), $this->socialMediaSettings(), $this->socialMediaStyleSettings());
	}
	public function businessInfoSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_business_section', __( 'Business Info','goldenaqsinc' )),
			Field::make( 'text', 'tabby_business_phone_number', __( 'Phone Number','goldenaqsinc' ))->set_width( 50 ),
			Field::make( 'textarea', 'tabby_business_address', __( 'Business Info Address','goldenaqsinc' ))->set_width( 50 )
		);
	}
	public function socialMediaSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_social_media_section', __( 'Business Info Social Media','goldenaqsinc')),
			Field::make( 'complex', 'tabby_social_meida', 'Add Social Media Icon and Link' )
				->add_fields( array(
					Field::make( 'text', 'icon', 'Icon Class' ),
					Field::make( 'text', 'link', 'Social Link' ),
				) ),

		);
	}
	public function socialMediaStyleSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_social_media_styles_section', __( 'Business Info Social Media Styles','goldenaqsinc' )),
			Field::make( 'text', 'tabby_social_icon_height', __( 'Height','goldenaqsinc' ))
				->set_help_text( 'Please enter value with unit. e.g. 20px or 1rem')
				->set_width( 33 ),
			Field::make( 'text', 'tabby_social_icon_width', __( 'Width','goldenaqsinc' ))
				->set_help_text( 'Please enter value with unit. e.g. 20px or 1rem')
				->set_width( 33 ),
			Field::make( 'text', 'tabby_social_icon_font_size', __( 'Icon Font Size', 'goldenaqsinc' ))
				->set_help_text( 'Please enter value with unit. e.g. 16px or 1rem')
				->set_width( 33 ),
			Field::make( 'color', 'tabby_social_icon_color', __( 'Icon Color','goldenaqsinc' ))->set_width( 50 ),
			Field::make( 'color', 'tabby_social_icon_hover_color', __( 'Icon Hover Color','goldenaqsinc' ))->set_width( 50 ),
			Field::make( 'color', 'tabby_social_icon_bg_color', __( 'Icon Background Color','goldenaqsinc' ))->set_width( 50 ),
			Field::make( 'color', 'tabby_social_icon_bg_hover_color', __( 'Icon Background Hover Color' ))->set_width( 50 ),
		);
	}
	public function renderAddress()
	{
		ob_start();
		$address = carbon_get_theme_option('tabby_business_address');
		?>
		<div class="goldenaqsinc-business-address">
			<?php if(!empty($address)): ?>
			<p><?php _e(nl2br($address),'goldenaqsinc') ;?></p>
			<?php else: ?>
			<p><?php esc_html__('Your address','goldenaqsinc'); ?></p>
			<?php endif;?>
		</div>
		<?php
		return ob_get_clean();
	}
	public function renderPhoneNumber()
	{
		$phoneNumber = carbon_get_theme_option('tabby_business_phone_number');
		if(!empty($phoneNumber))
			return $phoneNumber;
		return 'Your phone number';
	}
	public function renderPhoneNumberLink()
	{
		ob_start();
		$phoneNumber = $this->renderPhoneNumber();
		?>
		<a href="tel:<?php echo $phoneNumber; ?>" class="goldenaqsinc-business-phone-link">
			<?php _e($phoneNumber, 'goldenaqsinc'); ?>
		</a>
		<?php
		return ob_get_clean();
	}
	public function renderSocialMediaLinks()
	{
		ob_start();
		$socialMediaLinks = carbon_get_theme_option('tabby_social_meida');
		if(!empty($socialMediaLinks)):
		?>
		<ul class="goldenaqsinc-social-links">
			<?php foreach ($socialMediaLinks as $socialLink): ?>
			<li>
				<a href="<?php echo $socialLink['link'];?>" target="_blank"><i class="<?php echo $socialLink['icon']; ?>"></i></a>
			</li>
			<?php endforeach; ?>
		</ul>
		<?php else: ?>
		<p><?php esc_html__('Your social links','goldenaqsinc'); ?></p>
		<?php
		endif;
		return ob_get_clean();
	}

}