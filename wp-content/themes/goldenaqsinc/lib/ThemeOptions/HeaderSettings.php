<?php
namespace Tabby\ThemeOptions;

use Carbon_Fields\Field;

class HeaderSettings
{
	public $settingsFields;

	public function __construct()
	{
		$this->settingsFields = $this->renderSettingsFields();
	}
	public function renderSettingsFields()
	{
		return array_merge($this->headerSettings(),$this->headerHeightSettings(),$this->headerMenuDesktopSettings());
	}
	public function headerSettings()
	{
		return array(
			Field::make('separator', 'tabby_header_cta_btn_title', __('Header Cta button style')),
			Field::make( 'select', 'tabby_header_cta_btn_style', __( 'Button style' ) )
				->set_options( array(
					'rectangle' => 'Rectangle',
					'oval' => 'Oval'
				)),
			Field::make('color', 'tabby_header_cta_btn_bg_color', __('Button Background'))->set_width(50),
			Field::make('color', 'tabby_header_cta_btn_bg_hover_color', __('Button Hover Background'))->set_width(50),
			Field::make('color', 'tabby_header_cta_btn_bg_text_color', __('Button Text Color'))->set_width(50),
			Field::make('color', 'tabby_header_cta_btn_bg_text_hover_color', __('Button Text Hover Color'))->set_width(50),
			Field::make( 'select', 'tabby_header_cta_btn_border_bottom', __('Button border '))
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					)
				))
				->set_options( array(
					'yes' => 'Yes',
					'no' => 'No'
				))->set_default_value('no'),
			Field::make('color', 'tabby_header_cta_btn_border_btn_color', __('Border bottom Color'))
				->set_width(50)
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_header_cta_btn_border_btn_hover_color', __('Border bottom Hover Color'))
				->set_width(50)
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
			Field::make('text', 'tabby_header_cta_btn_border_btn_height', __('Border bottom Height'))
				->set_width(50)
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

//			outside border style
			Field::make( 'select', 'tabby_header_cta_btn_border_outside', __('Outside border'))
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					)
				))
				->set_options( array(
					'yes' => 'Yes',
					'no' => 'No'
				))->set_default_value('no'),
			Field::make('color', 'tabby_header_cta_btn_border_outside_btn_color', __('Border  Color'))
				->set_width(50)
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_header_cta_btn_border_outside_btn_hover_color', __('Border  Hover Color'))
				->set_width(50)
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
			Field::make('text', 'tabby_header_cta_btn_border_outside_width', __('Border  Height'))
				->set_width(50)
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_header_cta_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_header_cta_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
		);
	}
	public function headerMenuDesktopSettings()
	{
		return array(
			Field::make('separator', 'tabby_header_main_bg_section', __('Header Sub Menu Styles For Desktop')),
			Field::make('color', 'tabby_header_main_desktop_bg', __('Background'))->set_width(50),
			Field::make('color', 'tabby_header_main_desktop_bg_hover', __('Background Hover'))->set_width(50),
			Field::make('color', 'tabby_header_main_desktop_text_color', __('Text Color'))->set_width(50),
			Field::make('color', 'tabby_header_main_desktop_text_hover', __('Text Color Hover'))->set_width(50),

		);
	}

	public function headerHeightSettings()
	{
		return array(
			Field::make('separator', 'tabby_header_height_section', __('Header Height Settings for Desktop')),
			Field::make('text', 'tabby_header_height_top', __('Top Header Height')),
			Field::make('text', 'tabby_header_height_main', __('Main Header Height')),
			Field::make('text', 'tabby_header_height_page_header', __('Page Header Height')),
			Field::make('separator', 'tabby_header_height_mobile', __('Header Height Settings for Mobile')),
			Field::make('text', 'tabby_header_height_main_mobile', __('Main Header Height')),
			Field::make('text', 'tabby_header_height_page_header_mobile', __('Page Header Height')),

		);
	}

}
