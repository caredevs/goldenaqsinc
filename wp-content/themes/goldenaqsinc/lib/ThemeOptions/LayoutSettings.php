<?php
namespace Tabby\ThemeOptions;

use Carbon_Fields\Field;

class LayoutSettings
{
	public $settingsFields;

	public function __construct()
	{
		include_once(ABSPATH . 'wp-admin/includes/plugin.php');
		$this->settingsFields = $this->renderSettingsFields();
	}
	public function renderSettingsFields()
	{
		return array_merge(
			$this->headerSetting(),
			$this->mobileMenuSetting(),
			$this->subscribeFormSetting(),
			$this->defaultArchiveSetting(),
			$this->teamStaffArchiveSetting(),
			$this->conditionsArchiveSetting(),
			$this->servicesArchiveSetting()
		);
	}
	public function headerSetting()
	{
		return array(
			Field::make('separator', 'tabby_header_section', __('Header')),
			Field::make('radio_image', 'tabby_header_layout', __('Choose Header Layout'))
				->set_options(array(
					'layout-1' => get_template_directory_uri().'/assets/dist/img/header-style-1.jpg',
					'layout-2' => get_template_directory_uri().'/assets/dist/img/header-style-2.jpg',
					'layout-3' => get_template_directory_uri().'/assets/dist/img/header-style-3.jpg',
					'layout-4' => get_template_directory_uri().'/assets/dist/img/header-style-4.jpg',
				))
				->set_classes('fullwidth-layout-options')
				->set_width(100),
			Field::make('checkbox', 'tabby_sticky_header', __('Sticky header'))
				->set_option_value('yes')
		);
	}
	public function mobileMenuSetting()
	{
		return array(
			Field::make('separator', 'tabby_mobile_menu_section', __('Mobile Menu')),
			Field::make('radio', 'tabby_menu_layout', __('Choose mobile menu layout'))
				->set_options(array(
					'hamburger' => __('Hamburger In Header', 'tabby'),
					'tabbar_top' => __('Tab Bar on Top'),
					'tabbar_bottom' =>  __('Tab Bar on Bottom'),
				)),
			Field::make('separator', 'tabby_alternate_mobile_menu_section', __('Alternate Mobile Menu')),
			Field::make('checkbox', 'tabby_alternate_menu_layout', __('Choose alternate mobile menu'))
				->set_option_value('yes')
				->set_help_text('Alternate menu will work with Tabbar menu and hamburger menu'),
		);
	}
	public function subscribeFormSetting()
	{
		return array(
			Field::make('separator', 'tabby_subscribe_form_section', __('Subscribe Form Layout')),
			Field::make('radio_image', 'tabby_subscribe_form_layout', __('Choose Header Layout'))
				->set_options(array(
					'none' => get_template_directory_uri().'/assets/dist/img/subscribe-style-none.jpg',
					'layout-1' => get_template_directory_uri().'/assets/dist/img/subscribe-style-1.jpg',
					'layout-2' => get_template_directory_uri().'/assets/dist/img/subscribe-style-2.jpg',
					'layout-3' => get_template_directory_uri().'/assets/dist/img/subscribe-style-3.jpg',
				))
				->set_classes('fullwidth-layout-options')
				->set_width(100),
		);
	}
	public function defaultArchiveSetting()
	{
		return array(
			Field::make( 'separator', 'tabby_default_archive_section', __('Default Archive')),
			Field::make( 'radio_image', 'tabby_default_archive_layout', __( 'Choose Default Archive Layout' ) )
				->set_options( array(
					'layout-1' => get_template_directory_uri().'/assets/dist/img/team-style-2.jpg',
					'layout-2' => get_template_directory_uri().'/assets/dist/img/team-style-3.jpg',
					'layout-3' => get_template_directory_uri().'/assets/dist/img/conditions-style-1.jpg',
					'layout-4' => get_template_directory_uri().'/assets/dist/img/conditions-style-2.jpg',
					'layout-5' => get_template_directory_uri().'/assets/dist/img/services-style-3.jpg'
				) )
				->set_classes('fullwidth-layout-options')
				->set_width(100),
		);
	}
	public function teamStaffArchiveSetting()
	{
		$active = is_plugin_active('staff/plugin.php');
		if ($active) {
			return array(
				Field::make('separator', 'tabby_staff_archive_section', __('Team/Staff Archive')),
				Field::make('radio_image', 'tabby_staff_archive_layout', __('Choose Team/Staff Archive Layout'))
					->set_options(array(
						'layout-1' => get_template_directory_uri().'/assets/dist/img/team-style-1.jpg',
						'layout-2' => get_template_directory_uri().'/assets/dist/img/team-style-2.jpg',
						'layout-3' => get_template_directory_uri().'/assets/dist/img/team-style-3.jpg'
					))
					->set_classes('fullwidth-layout-options')
					->set_width(100),
			);
		}
		return [];
	}
	public function conditionsArchiveSetting()
	{
		$active = is_plugin_active('conditions/plugin.php');
		if ($active) {
			return array(
				Field::make('separator', 'tabby_conditions_archive_section', __('Conditions Archive')),
				Field::make('radio_image', 'tabby_conditions_archive_layout', __('Choose Conditions Archive Layout'))
					->set_options(array(
						'layout-1' => get_template_directory_uri().'/assets/dist/img/conditions-style-1.jpg',
						'layout-2' => get_template_directory_uri().'/assets/dist/img/conditions-style-2.jpg'
					))
					->set_classes('fullwidth-layout-options')
					->set_width(100),
				Field::make('radio', 'tabby_conditions_archive_testimonial', __('Show testimonials at bottom'))
					->set_options(array(
						'yes' => 'Yes',
						'no' => 'No'
					))
			);
		}
		return [];
	}
	public function servicesArchiveSetting()
	{
		if (is_plugin_active('services/plugin.php')) {
			return array(
				Field::make('separator', 'tabby_services_archive_section', __('Services Archive')),
				Field::make('radio_image', 'tabby_services_archive_layout', __('Choose Services Archive Layout'))
					->set_options(array(
						'layout-3' => get_template_directory_uri().'/assets/dist/img/services-style-3.jpg',
						'layout-1' => get_template_directory_uri().'/assets/dist/img/conditions-style-1.jpg',
						'layout-2' => get_template_directory_uri().'/assets/dist/img/conditions-style-2.jpg'
					))
					->set_classes('fullwidth-layout-options')
					->set_width(100),
				Field::make('radio', 'tabby_services_archive_testimonial', __('Show testimonials at bottom'))
					->set_options(array(
						'yes' => 'Yes',
						'no' => 'No'
					))
			);
		}
		return [];
	}
}
