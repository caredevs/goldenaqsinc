<?php
namespace GoldenAqsInc;
Class CustomPostType{

	public function __construct()
	{
		add_action('init', [ $this, 'teamMembersInit' ]);

		add_action( 'manage_team_members_posts_custom_column', [$this, 'teamMembersColumns'], 10, 9 );
		add_filter('manage_edit-team_members_columns', [ $this, 'myEditTeamMembersColumns' ]);
	}

	/*
	 * custom post type Teams
	 */
	public function teamMembersInit()
	{

		register_post_type('team_members', [
			'labels'            => [
				'name'                      => 'Teams Members',
				'singular_name'     => 'Team Member',
				'all_items'             => 'All Team Member',
				'add_new'                   => 'Add New Team Member',
				'add_new_item'      => 'Add New Team Member'
			],
			'public'            => true,
			'has_archive'   => true,
			'menu_icon'           => 'dashicons-groups',
			'hierarchical'       => false,
			'rewrite'           => [ 'slug' => 'team-members' ],
			'supports'      => [ 'title','editor','thumbnail'],
			'show_ui' => true,
		]);
	}
	public function myEditTeamMembersColumns( $columns )
	{
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __('Team Member Name', 'ornage'),
			'image' => __('Team Member Images', 'ornage'),
			'testimonials_desc' => __('Team Member Short Desc', 'ornage'),
			'author' => __('Author', 'ornage'),
			'date' => __('Date', 'ornage')
		);
		return $columns;
	}
	public function teamMembersColumns($column, $post_id)
	{
		global $post;

		switch ($column)
		{
			case 'image':
				$img = get_post_meta($post_id, '_thumbnail_id', true );
				if(!empty($img)):
					echo '<img src="'. wp_get_attachment_image_url($img, 'thumbnail' ).'" width="80" height="auto">';
				endif;
				break;

			case 'testimonials_desc':
				$thecontent = get_the_content();
				if(!empty($thecontent)):
					echo substr($thecontent, 0, 70);
				endif;
				break;
		}
	}

}

new CustomPostType;