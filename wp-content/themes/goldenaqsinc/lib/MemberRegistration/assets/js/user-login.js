jQuery(function ($) {
    'use strict';
    $('.user-login').on('submit', function (e) {
        e.preventDefault();
        var loginInfo = $(this).serializeArray();

        $.ajax({
            method: 'POST',
            url: userLoginObj.ajax_url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', userLoginObj.nonce);
            },
            data: {
                action : 'UserLoginData',
                security: userLoginObj.nonce,
                userLoginInfo : loginInfo
            },
            success: function (r) {
                var response = $.parseJSON(r);
                if(response.Status === false){
                    $('.status-error').fadeIn(100).html('<p class="alert alert-danger">' + response.message + '</p>');
                }else{
                    $('.status-success').fadeIn(100).html('<p class="alert alert-success">' + response.message + '</p>');
                    window.location.href = response.redirectUrl;
                }
            },
            error: function (r) {
                console.log(r);

            },
            complete: function () {
                setTimeout(function () {
                    $('.status-success').fadeOut(100);
                }, 400);
            }
        });

    });
});
