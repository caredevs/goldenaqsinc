jQuery(function ($) {
    'use strict';



    $('.member-register').on('submit', function (e) {
        e.preventDefault();

        var formSerializeArray = $(this).serializeArray();

        $.ajax({
            method: 'POST',
            url: RegistrationObj.ajax_url,
            beforeSend: function (xhr) {
                $('.member-registration .register').css('opacity','0.5');
                xhr.setRequestHeader('X-WP-Nonce', RegistrationObj.nonce);
            },
            data: {
                action : 'MemberRegister',
                security: RegistrationObj.nonce,
                formData : formSerializeArray
            },
            success: function (r) {
                var response = $.parseJSON(r);

                if(response.Status === false){
                    if ( response.message.hasOwnProperty('company') ) {
                        $('#emptyCompany').html( response.message.company);
                    }
                    if ( response.message.hasOwnProperty('firstName') ) {
                        $('#emptyFirstName').html( response.message.firstName);
                    }
                    if ( response.message.hasOwnProperty('lastName') ) {
                        $('#emptyLastName').html( response.message.lastName);
                    }

                    if ( response.message.hasOwnProperty('email') ) {
                        $('#emptyEmail').html( response.message.email);
                    }
                }else{
                    $('.status-success').fadeIn(100).html('<p class="alert alert-success">' + response.message + '</p>');
                    window.location.href = response.redirectUrl;
                }
            },
            error: function (r) {
                console.log($.parseJSON(r));
            },
            complete: function () {
	            $('.member-registration .register').css('opacity','1');
                setTimeout(function () {
                    $('.status-success').fadeOut(100);
                }, 400);
            }
        });
    });


});
