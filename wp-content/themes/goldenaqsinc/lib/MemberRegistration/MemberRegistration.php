<?php
/**
 * MemberRegistration
 * Class
 * @var
 */
class MemberRegistration
{
	public function __construct()
	{
		$this->initMemberRegistration();
	}
	public function initMemberRegistration()
	{
		require_once __DIR__. '/RegisterMembers/RegisterMembers.php';
	}
}
new MemberRegistration();
