<?php get_header(); 
if ( ! is_front_page() ) :
	get_template_part('template-parts/banner-layout-1');
endif;
?>
<?php if(!is_user_logged_in()): ?>
<div  class="member-registration mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 offset-md-2">
				<div class="register">
					<div class="status-success"></div>
					<div class="row ">
						<div class="col-12">
							<div class="register-heading text-center">
								<h3 class="font-weight-normal">
									<span class="d-block font-weight-bold"><?php _e('Golden Aqs Inc Registration Form', 'goldenaqsinc'); ?></span>
									<?php _e('Tell us what you need so you can meet your virtual assistant.', 'goldenaqsinc'); ?></h3>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="register-form mx-auto">
								<div class="status-error"></div>

								<form  accept-charset="utf-8" class="member-register" enctype="multipart/form-data">
									<div class="row mb-4">
										<div class="col-xs-6 col-md-6">
											<div class="form-group">
												<select name="business" class="form-control rounded-0 business" id="business" >
													<option value="For my business">For my business</option>
													<option value="For my business">For my home</option>
												</select>
											</div>
										</div>
										<div class="col-xs-6 col-md-6">
											<div class="form-group">
												<input type="text" name="company" placeholder="Company" class="form-control rounded-0" id="company">
												<span id="emptyCompany" class="text-danger"></span>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-xs-6 col-md-6">
											<div class="form-group">
												<input type="text" name="firstName" placeholder="First Name" class="form-control rounded-0 firstName" id="firstName">
												<span id="emptyFirstName" class="text-danger"></span>
											</div>
										</div>
										<div class="col-xs-6 col-md-6">
											<div class="form-group">
												<input type="text" name="lastName" placeholder="Last Name" class="form-control rounded-0 lastName" id="lastName">
												<span id="emptyLastName" class="text-danger"></span>
											</div>
										</div>
									</div>
									<div class="row mb-4">
										<div class="col-12">
											<div class="form-group">
												<input type="email" name="email" placeholder="Email" class="form-control rounded-0 email" id="email">
												<span id="emptyEmail" class="text-danger"></span>
											</div>
										</div>
									</div>

									<div class="row mb-3 mt-3">
										<div class="col-12">
											<div class="register-btn-wrapper text-left form-group">
<!--												<button class="btn btn-lg btn-primary signup-btn rounded-0" type="submit">Register</button>-->
												<input type="submit" name="register" value="Register">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php else: ?>
<div  class="member-registration mt-5 mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="already-register text-center">
					<h1 class="py-5">You Already Have an Account</h1>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>