<?php
get_header();
if ( ! is_front_page() ) :
	get_template_part('template-parts/banner-layout-1');
endif;
?>

<div  class="member-update-password my-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="register">

					<div class="row">
						<div class="col-12">
							<div class="register-form mx-auto my-5">

									

								<form  accept-charset="utf-8" class="update-user-password">
								    
								    <input type="hidden" name="userloginName" id="userloginName"
						value="<?php 
                echo $_GET['login'];
                ?>
" autocomplete="off"/>
                                    <input name="userVerifyKey" type="hidden" id="userVerifyKey"
						value="<?php 
                echo $_GET['key'];
                ?>
" autocomplete="off"/>
									<div class="row mb-3">
										<div class="col-12 offset-md-3 col-md-6">
											<div class="form-label">Enter your new password below.</div>
											<div class="update-pass bg-white p-5">
    									    <div class="status-error text-center pb-2 text-danger">
    									    </div>	    
        									<div class="status-success text-center pb-2 text-success">
        									</div>
												<div class="er-field-wrap">
													<label for="name" class="float-left">New password</label>
													<div class="newPassField">
														<input type="password" name="password" class="form-control input-lg userPassword" id="userPassword" />
														<i id="showPassword" class="fa fa-eye fa-2x"></i>
														<i id="hiddenPassword" class="fa fa-eye-slash fa-2x"></i>
													</div>
													<span id="password-strength"></span>
													<div class="resetPassweak pw-weak pt-2" style="display: none;">
														<label>
															<input type="checkbox" name="userSetWeakPass" class="pw-checkbox userSetWeakPass"> Confirm use of weak password
														</label>
													</div>
													<p class="pt-2">Hint: The password should be at least twelve characters long. To make it stronger, use upper and lower case letters, numbers, and symbols like ! " ? $ % ^ & ).</p>
													
												</div>
												<div class="register-btn-wrapper text-left mt-3">
																										<input type="submit" disabled="disabled" value="Reset Password" class="btn btn-lg btn-primary signup-btn rounded-0"/>
												</div>
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>