<?php
namespace MemberRegister;

class RegisterProcess{

	protected $errors = array();
	public function __construct() {
		add_action( 'wp_ajax_MemberRegister', [$this, 'MemberRegisterProcess']);
		add_action( 'wp_ajax_nopriv_MemberRegister', array($this, 'MemberRegisterProcess' ));
	}

	/*
	 * MemberRegister ajax
	 * @mixed
	 */
	public function MemberRegisterProcess(){

		if(!check_ajax_referer( 'register-member-form-nonce', 'security', false)){
			echo 'Nonce not varified';
			wp_die();
		}else{

			if( isset( $_POST[ 'formData' ] ) ) {
				$fieldsKeyName = array();
				$fieldsValue = array();

				foreach ($_POST['formData'] as $formField) {
					$fieldsKeyName[] = $formField['name'];
					$fieldsValue[] = $formField['value'];
				}

				$registerData = array_combine($fieldsKeyName, $fieldsValue);
				$errors = array();
				if( empty( $registerData['company'] ) )
					$errors['company'] = 'Enter Company Name';

				if( empty( $registerData['firstName'] ) )
					$errors['firstName'] = 'Enter First Name';

				if( empty( $registerData['lastName'] ) )
					$errors['lastName'] = 'Enter Last Name';

				if( empty( $registerData['email'] ) )
					$errors['email'] = 'Enter Email Address';
				elseif( !filter_var($registerData['email'], FILTER_VALIDATE_EMAIL) )
					$errors['email'] = 'Enter Valid Email';

				if( empty( $errors ) ) {
					if (!email_exists($registerData['email'])) {
						$userMetas = array(
							'business' => $registerData['business'],
							'company' => $registerData['company'],
							'firstName' => $registerData['firstName'],
							'lastName' => $registerData['lastName'],
							'email' => $registerData['email'],
							'status' => 0
						);
						$randomNumber = mt_srand(3);
						$randompassword = wp_generate_password(3);
						$userId = wp_create_user($userMetas['lastName'].$randomNumber, $randompassword, $userMetas['email']);
						if ($userId != NULL) {
							foreach ($userMetas as $userMetaKey => $userMetaValue) {
								update_user_meta($userId, $userMetaKey, $userMetaValue);
							}
						}
						$userInfo  = get_userdata($userId);
						if($userId != NULL){
							$registeredUserInfo = array(
								'userName' => $userInfo->user_login,
								'email' => $userInfo->user_email,
								'userId' => $userInfo
							);
							$this->MailSent($registeredUserInfo);
						}
						echo json_encode(array('Status' => true, 'message' => 'Registration Success', 'redirectUrl' => home_url() . '/Thank-you'));
						wp_die();
					} else {
						echo json_encode(array('Status' => false, 'message' => 'Email address already exits'));
						wp_die();
					}
				} else{
					echo json_encode(array('Status' => false, 'message' =>  $errors));
					wp_die();
				}
			}
		}
	}

	public function MailSent($registeredUserInfo){
	    
		add_filter( 'wp_mail_content_type', function( $content_type ) {
			return 'text/html';
		});

 		$subject = __("Your account Password Create Link ".get_bloginfo( 'name'));
 		$headers = array();
		$userData = $registeredUserInfo['userId'];
		$userLogin = $registeredUserInfo['userName'];
	    $adt_rp_key = get_password_reset_key( $registeredUserInfo['userId'] );
	    
	   $rp_link = '<a href="' . home_url()."/member-reset-password/?key=$adt_rp_key&login=" . rawurlencode($registeredUserInfo['userName']) . '">' . home_url()."/member-reset-password/?key=$adt_rp_key&login=" . rawurlencode($registeredUserInfo['userName']) . '</a>';
	    
	    
	    // $rp_link = home_url(). "/member-reset-password?action=rp&key=$key&login=$userLogin";
	    
		$message = "Hi ".$registeredUserInfo['firstName']."<br>";
		$message .= "Your account has been created on ".get_bloginfo( 'name' );
		$message .= __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
		$message .= "<br>";
		$message .= sprintf(__('Username: %s'), $registeredUserInfo['userName']) . "\r\n\r\n";
		$message .= "<br>";
		$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
		$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
		$message .= "<br>";
        $message .= $rp_link.'<br>';
		$message .= "<br>";
		$message .= "Thank you";
 		$headers[] = 'From: '.get_bloginfo( 'name').'<'.get_option('admin_email').'>'. "\r\n";

		wp_mail( $registeredUserInfo['email'],$subject,$message,'sdfsdfds');
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}
}