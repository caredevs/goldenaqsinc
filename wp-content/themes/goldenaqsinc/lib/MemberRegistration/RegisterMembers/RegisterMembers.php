<?php
/**
 * RegisterMembers
 * Class
 * @var
 */

require_once __DIR__ . '/RegisterProcess.php';
require_once __DIR__ . '/userProfileUpdateProcess.php';
require_once __DIR__ . '/UserPasswordUpdate.php';
require_once __DIR__ . '/UserLogin.php';

class RegisterMembers
{
	/**
	 * assets directory
	 * @var string
	 */
	protected $assetsUrl;

	/**
	 * Template directory
	 * @var string
	 */
	protected $templates;

	/**
	 * saved general settings
	 * @var array
	 */
	private $memberData;

	/**
	 * slug for RegisterForm
	 * @var string
	 */
	protected $slug = 'registerMember';
	protected $slugPasswordUpdate = 'userPasswordUpdate';
	protected $slugUserLogin = 'userLogin';
	protected $slugUserProfileUpdate = 'userProfileUpdate';
	//protected $slugUserApprove = 'userApprove';



	/**
	 * method __construct()
	 * set values in properties
	 * @uses add_action(), add_shortcode() wp functions
	 */
	public function __construct()
	{
		$this->assetsUrl = get_template_directory_uri() . '/lib/MemberRegistration/assets/';
		$this->templates = get_stylesheet_directory() . '/lib/MemberRegistration/templates/';

		add_action('init', [$this, 'UserRegistrationProcessInit']);
	//	add_action('admin_init', [$this, 'UserListTableApproveInit']);
		add_action('wp_enqueue_scripts', array( $this, 'registrationEnqueueScripts' ));
		add_action('admin_enqueue_scripts', array( $this, 'ApproveUserAdminEnqueueScripts' ));
		add_filter('theme_page_templates', array( $this, 'registerMemberAddPageTemplates' ));
		add_filter('page_template', array( $this, 'registerRedirectPageTemplates' ));

	}


	/**
	 * initialize RegistrationProcessInit
	 * @uses UserRegistrationProcessInit class
	 * MemberRegister namespace
	 */

	public function UserRegistrationProcessInit()
	{
		new MemberRegister\RegisterProcess();
		new MemberRegister\UserPasswordUpdate();
		new MemberRegister\UserLogin();
		new MemberRegister\userProfileUpdateProcess();
	}

	/**
	 * initialize UserListTableInit
	 * @uses UserListTable class
	 * MemberRegister namespace
	 */

	public function UserListTableApproveInit()
	{
		new MemberRegister\UserListTableApprove();
	}

	/*
	 * wp enqueue script
	 * @mixed
	 */
	public function registrationEnqueueScripts() {

		wp_enqueue_script( 'password-strength-meter' );
		wp_enqueue_media();

		wp_enqueue_script($this->slug . '-scripts', $this->assetsUrl . 'js/registration.js', ['jquery'],'', true);

		wp_localize_script($this->slug . '-scripts', 'RegistrationObj', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce'    => wp_create_nonce('register-member-form-nonce')
		));

		wp_enqueue_script($this->slugPasswordUpdate . '-scripts', $this->assetsUrl . 'js/user-password-update.js', ['jquery'],'', true);

		wp_localize_script($this->slugPasswordUpdate . '-scripts', 'userPasswordUpdateObj', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce'    => wp_create_nonce('user-password-update-form-nonce'),
			'empty' => __( 'Strength indicator' ),
			'short' => __( 'Very weak' ),
			'bad' => __( 'Weak' ),
			'good' => _x( 'Medium', 'password strength' ),
			'strong' => __( 'Strong' ),
			'mismatch' => __( 'Mismatch' )
		));

		wp_enqueue_script($this->slugUserLogin . '-scripts', $this->assetsUrl . 'js/user-login.js', ['jquery'],'', true);

		wp_localize_script($this->slugUserLogin . '-scripts', 'userLoginObj', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce'    => wp_create_nonce('user-login-form-nonce')
		));
		
		
		wp_enqueue_script($this->slugUserProfileUpdate . '-script', $this->assetsUrl . 'js/user-profile-update.js', ['jquery'],'', true);

		wp_localize_script($this->slugUserProfileUpdate . '-script', 'userProfileUpdateObj', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce'    => wp_create_nonce('user-profile-update-form-nonce')
		));

	}


	/*
	 * wp Admin enqueue script
	 * @mixed
	 */

	public function ApproveUserAdminEnqueueScripts() {

		wp_enqueue_script($this->slugUserApprove . '-scripts', $this->assetsUrl . 'js/user-approve.js', ['jquery'],'', true);

		wp_localize_script($this->slugUserApprove . '-scripts', 'UserApproveObj', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'nonce'    => wp_create_nonce('user-approve-nonce')
		));
	}

	/*
	 * page Template files added
	 * wp-admin Template name
	 * @mixed
	 */
	public function registerMemberAddPageTemplates ($templates) {
		$templates['registration.php'] = 'Member Registration';
		$templates['login.php'] = 'Member Login';
		$templates['reset-password.php'] = 'Member Reset Password';
		$templates['register-member-profile.php'] = 'Member Profile ';
		$templates['user-profile-update.php'] = 'Member Profile Update Settings';
		return $templates;
	}


	/*
	 * Redirect Templates
	 * registerRedirectPageTemplates
	 * @mixed
	 */
	public function registerRedirectPageTemplates ($template) {

		if (is_page_template('registration.php'))
			$template =   $this->templates . 'registration.php';

		if (is_page_template('login.php'))
			$template = $this->templates . 'login.php';

		if (is_page_template('reset-password.php'))
			$template = $this->templates . 'reset-password.php';

		if (is_page_template('register-member-profile.php'))
			$template = $this->templates . 'register-member-profile.php';

		if (is_page_template('user-profile-update.php'))
			$template = $this->templates . 'user-profile-update.php';

		return $template;
	}

}

$registerMember = new RegisterMembers();
