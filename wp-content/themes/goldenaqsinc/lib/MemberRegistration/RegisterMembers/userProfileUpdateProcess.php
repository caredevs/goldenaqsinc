<?php
namespace MemberRegister;


class userProfileUpdateProcess{


	public function __construct() {
	    add_action( 'wp_ajax_userProfileupdate', array($this, 'updateProfileProcess'));
	    add_action( 'wp_ajax_nopriv_userProfileupdate', array($this, 'updateProfileProcess'));
	}

	/*
	 * updateMemberProfile ajax
	 * @mixed
	 */
	public function updateProfileProcess(){

		if(!check_ajax_referer( 'user-profile-update-form-nonce', 'security', false)){
			echo 'Nonce not varified';
			wp_die();
		}
		else{

			if( isset( $_POST[ 'userUpdateProfile' ] ) ) {

				$data = array();
				$metaUserValue = array();

				foreach ($_POST['userUpdateProfile'] as $key => $value) {
					$data[] = $value['name'];
					$metaUserValue[] = $value['value'];
				}
				$registerData = array_combine($data, $metaUserValue);
				if (!empty($registerData)) {

					$metas = array(
						'name' => $registerData['name'],
						'nickName' => $registerData['nickName'],
						'qualification' => $registerData['qualification'],
						'department' => $registerData['department'],
						'session' => $registerData['session'],
						'hostelStayPeriod' => $registerData['hostelStayPeriod'],
						'lastHostelLivingRoom' => $registerData['lastHostelLivingRoom'],
						'dateOfBirth' => $registerData['dateOfBirth'],
						'bloodGroup' => $registerData['bloodGroup'],
						'nationalIdNumber' => $registerData['nationalIdNumber'],
						'occupationDesignation' => $registerData['occupationDesignation'],
						'fatherName' => $registerData['fatherName'],
						'motherName' => $registerData['motherName'],
						'spouseName' => $registerData['spouseName'],
						'childrenNumber' => $registerData['childrenNumber'],
						'son' => $registerData['son'],
						'daughter' => $registerData['daughter'],
						'phoneOfficeHome' => $registerData['phoneOfficeHome'],
						'presentAddress' => $registerData['presentAddress'],
						'permanentAddress' => $registerData['permanentAddress'],
						'mobile' => $registerData['mobile'],
						'email' => $registerData['email'],
						'status' => 0
					);

					$userId = get_current_user_id();

					if ($userId != NULL) {
						foreach ($metas as $key => $value) {
							update_user_meta($userId, $key, $value);
						}
					}
					echo json_encode(array('Status' => true, 'message' => 'Update Profile Success'));
					wp_die();
				} else {
					echo json_encode(array('Status' => false, 'message' => 'Update Profile Fails'));
					wp_die();
				}
			}
		}

	}
	
	public function custom_blockusers_init() {
  if ( is_admin() && !defined('DOING_AJAX') && ( 
  current_user_can('usercrp') || current_user_can('userpcp') ||  
  current_user_can('subscriber') || current_user_can('contributor') || 
  current_user_can('editor'))) {
    session_destroy();
    wp_logout();
    wp_redirect( home_url() );
   exit;
  }
 }

}

