<?php
namespace MemberRegister;
class UserPasswordUpdate{


	public function __construct() {
		add_action( 'wp_ajax_userPasswordUpdate', [$this, 'userPasswordUpdateProcess']);
		add_action( 'wp_ajax_nopriv_userPasswordUpdate', array($this, 'userPasswordUpdateProcess' ));
	}

	/*
	 * updateMemberProfile ajax
	 * @return void
	 */
	public function userPasswordUpdateProcess(){

		if(!check_ajax_referer( 'user-password-update-form-nonce', 'security', false)){
			echo 'Nonce not varified';
			wp_die();
		}
		else{
		
			if( !empty( $_POST[ 'resetPassInfo' ] ) ) {
			    
                 $userKey = array();
                 $userValue = array();
                
				foreach ($_POST['resetPassInfo'] as $key => $value) {
					$userKey[] = $key;
					$userValue[] = $value;
				}
			
				$userLoginInfo = array_combine($userKey, $userValue);
	            
                $userData = check_password_reset_key($userLoginInfo[1]['value'], $userLoginInfo[0]['value']);

				$status='';
				if ( is_wp_error($userData) ):
					if ( $userData->get_error_code() === 'expired_key' ):
						$status = 'Invalid Key Pasword Rest Link' ;
					else:
						$status = 'Expired Time Pasword Rest Link' ;
					endif;
					
					echo json_encode([
					'Status' => false,
					'message' => $status
					]);
				   	wp_die();
				
				else:
				  
					if (!empty($userData)):
						reset_password($userData, $userLoginInfo[2]['value']);
					    $this->MailSentnewPassword($userData, $userLoginInfo);
					    
					    echo json_encode(array('Status' => true, 'message' => 'Added New Password Success', 'redirectUrl' => home_url().'/login'));
						wp_die();
						
					else:
						echo json_encode(array('Status' => false, 'message' => 'Password Set Fails', ));	
						wp_die();
					endif;
					die;
				endif;
			}else{
			    echo json_encode(array('Status' => false, 'message' => 'Password Set Fails', ));	
				wp_die();
			    
			}
		}
	}
	
	/*
	 * @MailSentnewPassword
	 * Mail send New password
	 * @return mixed
	 */
	 
	public function MailSentnewPassword($userInfo, $loginInfo){
	    
	    $loginUserPass = $loginInfo[2]['value'];
	    $loginUserName = $loginInfo[0]['value'];

		add_filter( 'wp_mail_content_type', function( $content_type ) {
			return 'text/html';
		});

		$subject = __("Your account Login Access ".get_bloginfo( 'name'));
		$headers = array();
		$message = __('Your new Login access:') . "\r\n\r\n";
		$message .= network_home_url( '/login/' ) . "\r\n\r\n";
		$message .= "<br>";
		$message .= sprintf(__('Username: %s'), $loginUserName) . "\r\n\r\n";
		$message .= "<br>";
		$message .= sprintf(__('Password: %s'), $loginUserPass) . "\r\n\r\n";	
		$message .= "<br>";
		$message .= "Thank you";
		$headers[] = 'From: '.get_bloginfo( 'name').'<'.get_option('admin_email').'>'. "\r\n";

		wp_mail( $userInfo->user_email,$subject,$message, $headers);

		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}
}

