<?php
namespace MemberRegister;

class UserLogin{


	public function __construct() {
		add_action( 'wp_ajax_UserLoginData', [$this, 'UserLoginProcess']);
		add_action( 'wp_ajax_nopriv_UserLoginData', array($this, 'UserLoginProcess' ));
	}

	/*
	 * Login ajax
	 * @mixed
	 */
	public function UserLoginProcess(){

		if(!check_ajax_referer( 'user-login-form-nonce', 'security', false)){
			echo 'Nonce not varified';
			wp_die();
		}else{

			if( isset( $_POST[ 'userLoginInfo' ] ) ) {

				$loginInfo = $_POST[ 'userLoginInfo' ];
				$userLogin = array();

				$userLogin['user_login'] = $loginInfo[0]['value'];
				$userLogin['user_password'] =  $loginInfo[1]['value'];
				$userLogin['remember'] = $loginInfo[2]['value'];


				$login = wp_signon( $userLogin, false );
				$userId = wp_set_current_user($login);


				if ( is_wp_error($login) ){
					echo json_encode(array('Status'=>false, 'message'=>'User name and Password Incorrect'));
					wp_die();
				} else {
					wp_set_auth_cookie( $userId->ID, $userLogin['remember'] );
					echo json_encode(array('Status'=>true, 'message'=>'Login Successfully', 'redirectUrl' => home_url().'/services/'));
					wp_die();
				}
			}
		}
	}


}