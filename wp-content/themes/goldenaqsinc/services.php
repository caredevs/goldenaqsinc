<?php
/*Template Name: Services page */
get_header();
if ( ! is_front_page() ) :
	get_template_part('template-parts/banner-layout-1');
endif;
?>

	<?php if(is_user_logged_in()): ?>
	<section class="package-list py-5">
		<div class="container">
			<div class="row py-4">

				<!-- Table #1  -->
				<div class="col-xs-12 col-lg-4">
					<div class="card text-center package-item">
						<div class="card-header">
							<h3 class="display-2"><span class="currency">$</span>10<span class="period">/ 5 hours</span></h3>
						</div>
						<div class="card-block">
							<h4 class="card-title">
								Basic Plan
							</h4>
							<ul class="list-group">
								<li class="list-group-item">Ultimate Features</li>
								<li class="list-group-item">Responsive Ready</li>
								<li class="list-group-item">Visual Composer Included</li>
								<li class="list-group-item">24/7 Support System</li>
							</ul>
							<a href="#" class="btn btn-gradient mt-2">Choose Plan</a>
						</div>
					</div>
				</div>

				<!-- Table #1  -->
				<div class="col-xs-12 col-lg-4">
					<div class="card text-center package-item">
						<div class="card-header">
							<h3 class="display-2"><span class="currency">$</span>20<span class="period">/ 10 hours</span></h3>
						</div>
						<div class="card-block">
							<h4 class="card-title">
								Regular Plan
							</h4>
							<ul class="list-group">
								<li class="list-group-item">Ultimate Features</li>
								<li class="list-group-item">Responsive Ready</li>
								<li class="list-group-item">Visual Composer Included</li>
								<li class="list-group-item">24/7 Support System</li>
							</ul>
							<a href="#" class="btn btn-gradient mt-2">Choose Plan</a>
						</div>
					</div>
				</div>

				<!-- Table #1  -->
				<div class="col-xs-12 col-lg-4">
					<div class="card text-center package-item">
						<div class="card-header">
							<h3 class="display-2"><span class="currency">$</span>30<span class="period">/ 20 hours</span></h3>
						</div>
						<div class="card-block">
							<h4 class="card-title">
								Premium Plan
							</h4>
							<ul class="list-group">
								<li class="list-group-item">Ultimate Features</li>
								<li class="list-group-item">Responsive Ready</li>
								<li class="list-group-item">Visual Composer Included</li>
								<li class="list-group-item">24/7 Support System</li>
							</ul>
							<a href="#" class="btn btn-gradient mt-2">Choose Plan</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<?php endif; ?>
	<section id="resource">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 text-center pt-5 pb-2">
					<div class="primary-heading mb-5">
						<h1>Our Services</h1>
					</div>
					<p>Call, email, or text and engage a human, not a computer. Your Virtual Assistant is here for you to provide exceptional service - the same service they'd want to experience themselves.</p>
				</div>
			</div>
			<div class="row pt-3">
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
				<div class="col-lg-4 mb-4">
					<div class="resource-item text-center">
						<i class="far fa-envelope"></i>
						<h4>Resource Item</h4>
						<p>Make purchases on your behalf, after finding the right price and product</p>
					</div>
				</div>
			</div>
			<div class="row py-5">
				<div class="col">
					<div class="section-btn text-center">
						<a class="btn gaqsinc-btn-primary rounded-0 btn-lg text-white">See All Services</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php if(!is_user_logged_in()): ?>
	<section class="register-section pt-5 pb-5" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/a11.jpg)">
		<div class="container h-100">
			<div class="row h-100 d-flex align-items-center">
				<div class="col-12 col-lg-8">
					<div class="register-info pb-5 d-block">
						<span class="gaqsinc-primary-text d-block mb-3">Contact</span>
						<h1 class="gaqsinc-secondary-light-text">Tell us what you need so you can <span>meet your virtual assistant.</span></h1>
					</div>
					<div class="register-form">
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<select name="business" class="form-control rounded-0" id="select-business" >
										<option value="For my business">For my business</option>
										<option value="For my business">For my home</option>
									</select>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="text" name="company" placeholder="Company" class="form-control rounded-0">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="text" name="firstname" placeholder="First name" class="form-control rounded-0">
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="text" name="lastname" placeholder="Last name" class="form-control rounded-0">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="email" name="email" placeholder="Email" class="form-control rounded-0">
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<input type="submit" name="register" value="Register">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
<?php get_footer(); ?>