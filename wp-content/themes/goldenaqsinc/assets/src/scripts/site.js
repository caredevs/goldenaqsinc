(function($){

    'use-strict';

    var slider = $(".slider");

    $('.slider').on('init', function(e, slick) {
        var $firstAnimatingElements = $('div.slick-slide:first-child').find('[data-animation]');
        doAnimations($firstAnimatingElements);
    });

    $('.slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
        var $animatingElements = $('div.slick-slide[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
        doAnimations($animatingElements);
    });


    slider.slick({
            dots: true,
            arrows: false,
            vertical: true,
        });


    // slider.on('wheel', (function(e) {
    //     e.preventDefault();
    //
    //     if (e.originalEvent.deltaY < 0) {
    //         $(this).slick('slickNext');
    //     } else {
    //         $(this).slick('slickPrev');
    //     }
    // }));
    //



    function doAnimations(elements) {
        var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        elements.each(function() {
            var $this = $(this);
            var $animationDelay = $this.data('delay');
            var $animationType = 'animated ' + $this.data('animation');
            $this.css({
                'animation-delay': $animationDelay,
                '-webkit-animation-delay': $animationDelay
            });
            $this.addClass($animationType).one(animationEndEvents, function() {
                $this.removeClass($animationType);
            });
        });
    }

    $('.client-testimonials').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });
    $('.client-logo-wrap').slick({
        dots: true,
        infinite: true,
        centerPadding: '60px',
        speed: 100,
        slidesToShow: 6,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });
    $('.members').slick({
        dots: true,
        infinite: true,
        centerPadding: '60px',
        speed: 300,
        slidesToShow: 3,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });


    $(window).on('load',function(){
        var sectionId = window.location.search.slice(9);
        if(sectionId){
            $('html,body').animate({
                scrollTop: ($( '#'+sectionId).offset().top - 70)
            }, 500);
        }
    });
    $(window).on('scroll',function(){
        var windowPositionFromTop = $(window).scrollTop();
        if(windowPositionFromTop > 40){
            $('header').addClass('fixed');
        }else{
            $('header').removeClass('fixed');
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
})(jQuery);
