var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify-es').default,
    cleancss = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('gulp-babel'),
    theme = 'wp-content/themes/goldenaqsinc/assets/';

gulp.task('sass', function(){
    return gulp.src( theme+'src/styles/*.scss' )
        .pipe(sourcemaps.init())
        .pipe( sass() )
        .pipe( cleancss() )
        .pipe(sourcemaps.write(''))
        .pipe( gulp.dest( theme+'dist/css'));
});

gulp.task('js', function() {
    return gulp.src(theme+ 'src/scripts/*.js' )
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe( uglify())
        .pipe(sourcemaps.write(''))
        .pipe( gulp.dest( theme+'dist/js'));
} );

gulp.task('watch', function() {
    gulp.watch( theme+'src/styles/**/*.scss',  gulp.series('sass'));
    gulp.watch( theme+'src/scripts/*.js', gulp.series('js'));
});
